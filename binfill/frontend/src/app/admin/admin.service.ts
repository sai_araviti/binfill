import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { map } from 'rxjs/operators';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient ,ProgressSpinnerModule :ProgressSpinnerModule) { }


  //get all users

  getAll() {


    return this.http.get<any>(`${environment.apiUrl}/users/admin`, { params: { limit: '1000' } });


  }
  //search admin
  searchAll(search: string) {


    return this.http.get<any>(`${environment.apiUrl}/users/admin`, { params: { search: search, limit: '1000' } });


  }

  //get all companies

  getAllcompanies() {

   
    return this.http.get<any>(`${environment.apiUrl}/company/create_update_company`, {});


  }


  //Add user
  addadmin(user: { email: string, lastName: string, firstName: string },

    phone_number: string,
    country: string,
    province: string,
    city: string,
    postal_code: string,
    street: string) {
    return this.http.post<any>(`${environment.apiUrl}/users/admin`, { postal_code: postal_code, street: street, user: { email: user.email, last_name: user.lastName, first_name: user.firstName }, phone_number: phone_number, country: country, province: province, city: city, }
    )
      .pipe(map
        (data => {
        

        }
        )
        );
        
  }
//editadmin
editadmin(user: { email: string, lastName: string, firstName: string },

  phone_number: string,
  country: string,
  province: string,
  city: string,
  postal_code: string,
  street: string,
  id: number) {
     
  return this.http.put<any>(`${environment.apiUrl}/users/admin`+ '/' + id, { postal_code: postal_code, street: street, user: { email: user.email, last_name: user.lastName, first_name: user.firstName }, phone_number: phone_number, country: country, province: province, city: city, }
  )
    .pipe(map
      (data => {
        

      }
      )
      );
}


  //delete

  deleteadmin(id: number) {

    return this.http.delete<any>(`${environment.apiUrl}/users/admin` + '/' + id);

  }
  //get country
  getcountry() {

    return this.http.get<any>(`${environment.apiUrl}/utils/country`, { params: { limit: '250' } })
      .pipe(map
        (data => {
          
          return data;
        }
        ));
  }
  //get province
  getprovince(province: string) {

    return this.http.get<any>(`${environment.apiUrl}/utils/province`, { params: { country_id: province, limit: '1000' } })
      .pipe(map
        (data => {
          
          return data;
        }
        ));
  }
  //get state
  getcity(city: string) {
    return this.http.get<any>(`${environment.apiUrl}/utils/city`, { params: { province_id: city, limit: '1000' } })
      .pipe(map
        (data => {
          
          return data;
        }
        ));

  }

}
