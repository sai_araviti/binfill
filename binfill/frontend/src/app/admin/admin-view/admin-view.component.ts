import { AdminService } from './../admin.service';
import { BreadcrumbService } from './../../@theme/components/breadcrumb/breadcrumb.service';
import {Component, OnInit,EventEmitter, Output,Input,HostListener} from '@angular/core';
import {first} from 'rxjs/operators';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-admin-view',
  templateUrl: './admin-view.component.html',
  styleUrls: ['./admin-view.component.css']
})
export class AdminViewComponent implements OnInit {

  constructor( private breadcrumbService: BreadcrumbService, private adminservice:AdminService) {
    this.breadcrumbService.setItems([
      {label: 'Admin Page'},
    ]);

   }


  users = [];
  search: string;
  
  @Input() display: boolean;


 
  //@Input() sideBar: CreateUpdateAdminComponent;

  // @HostListener('click')
  // click() {
  //   this.sideBar.showDialog();
  // }



 
  ngOnInit() {

   this.loadAllUsers();

    
  }
  //toggleMenu():void {
  //  this.toggleMenu = !this.toggleMenu;
  //  this.toggleMenu.emit(this.toggleMenu);
  //}
    deleteadmin(value){
     

        
    }




  loadAllUsers() {
   
    this.adminservice.getAll().pipe(first()).subscribe(
      data1 => {
       
        this.users = data1.results;


      },
      error => {

        console.error(error);
      }
    );
  }


  //filter admin


  FilterAdmin() {
    this.adminservice.searchAll(this.search).pipe(first()).subscribe(
      data1 => {
        

        
        this.users = data1.results;
        


      },
      error => {

        console.error(error);
      }
    );


  }
  onclear() {
    this.search = '';
  }

}
