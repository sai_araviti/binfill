import { AdminService } from './../../admin.service';

import { SelectItem, MessageService } from 'primeng/api';
import { Component, OnInit,Input,HostBinding} from '@angular/core';
import {first} from 'rxjs/operators'
@Component({
  selector: 'app-admin-edit',
  templateUrl: './admin-edit.component.html',
  styleUrls: ['./admin-edit.component.css']
})
export class AdminEditComponent implements OnInit {
  AFname: string;
  ALname: string;
  AEmail: string;
  Aphone: string;
  AAdress: string;
  ACity: string;
  AProvince: string;
  ACountry: string;
  AStreet: string;
  APincode: string;
  totalRecords: number;
  totalRecords1: number;
  Asearch: string;


  //currentUser: User;
  users = [];
  companys = [];
  public headers = [];
  candidate: any;
  countrys: SelectItem[];
  selectedCountry: string;
  provinces: SelectItem[];
  selectedProvince: string;
  city: SelectItem[];
  selectedCity: string;
  countrySelected:string;



  private isButtonVisible = false;
  private issaveVisible = true;
  constructor( private adminservice: AdminService, private messages: MessageService) { }

 
  
  display: boolean = false;
  displaycompany: boolean = false;
 
 
  showDialog() {
  
   

    this.display = true;
    this.isButtonVisible = false;
    this.issaveVisible = true;
    
  }

  showCompany() {

    this.displaycompany = true;

   
  }

  ngOnInit() {

    this.loadAllUsers();
   // this.getvalues();
    //this.loadallcomp();


  }

  getProvinces($event) {
    
    
    this.selectedCountry =$event.id ;
    this.adminservice.getprovince($event.id).pipe(first()).subscribe(
      data => {
        

        this.provinces = [];


        for (let i = 0; i < data.results.length; i++) {
          //the property after data[i]. needs to match the exact name that is on your JSON file... So, name is a different property than Name
          this.provinces.push({label: data.results[i].name, value: data.results[i].id});

        }

        
        return this.provinces;

      },
      error => {

        console.error(error);
      }
    );

  }

  getCity($event) {
    
    

    this.adminservice.getcity($event).pipe(first()).subscribe(
      data1 => {
       

        this.city = [];


        for (let i = 0; i < data1.results.length; i++) {
          //the property after data[i]. needs to match the exact name that is on your JSON file... So, name is a different property than Name
          this.city.push({label: data1.results[i].name, value: data1.results[i].id});
        }

        
        return this.city;
      },
      error => {

        console.error(error);
      }
    );

  }


  loadAllUsers() {
   
    this.adminservice.getAll().pipe(first()).subscribe(
      data1 => {
       
        this.users = data1.results;


      },
      error => {

        console.error(error);
      }
    );
  }

  createadmin() {
    
    this.display = true;
    this.isButtonVisible = false;
    this.issaveVisible = true;
    let email = this.AEmail;
    let fname = this.AFname;
    let lname = this.ALname;

    this.adminservice.addadmin({
        email: email,
        firstName: fname,
        lastName: lname
      }, this.Aphone, this.selectedCountry, this.selectedProvince, this.selectedCity,
      this.APincode, this.AStreet)
      .pipe(first())
      .subscribe(
        data => {
          this.display = false;
          this.messages.add({key: 'tl', severity: 'success', summary: 'Admin Created', detail: 'Created new admin successfully'});
          this.loadAllUsers();
          
        },
        err => {

          this.messages.add({key: 'forget1', severity: 'error', summary: 'Error', detail: 'invalid entry' + err});
          
        });


  }


  // getvalues() {

  //   this.ViewserviceService.getcountry().pipe(first())
  //     .subscribe(
  //       data => {
  //         
  //         this.countrys = [];


  //         for (let i = 0; i < data.results.length; i++) {
  //           //the property after data[i]. needs to match the exact name that is on your JSON file... So, name is a different property than Name
  //           this.countrys.push({label: data.results[i].name, value: data.results[i].id});
  //         }

  //         return this.countrys;
  //       },

  //       error => {
  //         // this.messages.add({ key: 'forget', severity: 'error', summary: 'Email Not Found ', detail: 'Error' + 
  //         // '' })

  //       });
  // }

  deleteAdmin(candidate) {

    this.adminservice.deleteadmin(candidate['id']).pipe(first())
      .subscribe(
        data => {
        
          this.loadAllUsers();
        },
        error => {

        }
      );


  }

  editadmin(candidate) {
    this.issaveVisible = false;
    this.isButtonVisible = true;
   
    this.display = true;
  
    this.AFname = candidate.user['firstName'];
    this.ALname = candidate.user['lastName'];
    this.AEmail = candidate.user['email'];
    this.AStreet = candidate['street'];
    this.APincode = candidate['postalCode'];
    this.Aphone = candidate['phoneNumber'];
   
    this.city = [];
    this.city.push({label: candidate.city['name'], value: candidate.city['id']});

    this.provinces = [];
    this.provinces.push({label: candidate.province['name'], value: candidate.province['id']});
    this.selectedCountry = candidate.country['id'];
    this.selectedProvince = candidate.province['id'];
    this.selectedCity = candidate.city['id'];


  }

  updateuser() {

    let id = this.candidate['id'];
  
    let email = this.AEmail;
    let fname = this.AFname;
    let lname = this.ALname;
    this.adminservice.editadmin({
        email: email,
        firstName: fname,
        lastName: lname
      }, this.Aphone, this.selectedCountry, this.selectedProvince, this.selectedCity,
      this.APincode, this.AStreet, id)
      .pipe(first())
      .subscribe(
        data => {
          this.display = false;
          this.messages.add({key: 'tl', severity: 'success', summary: 'Admin upadted', detail: 'Created new admin successfully'});
          this.loadAllUsers();
          
        },
        err => {

          this.messages.add({key: 'forget1', severity: 'error', summary: 'Error', detail: 'invalid entry' + err});
         
        });
  }

  //load all companies


  loadallcomp() {

   
    this.adminservice.getAllcompanies().pipe(first()).subscribe(
      data1 => {
        
        this.companys = [];
        this.companys = data1.results;
        this.totalRecords1 = data1.results.length;
        
      },
      error => {

        console.error(error);
      }
    );


  }


//messages

  showConfirm(candidate) {
    
    let id = candidate.user['firstName'];
    this.messages.clear();
    this.messages.add({
      key: 'c',
      sticky: true,
      severity: 'warn',
      summary: 'Are you sure want to delete' + '\xa0' + id + '?',
      detail: 'Confirm to proceed'
    });

  

    this.candidate = candidate;
  
   


  }

  showEdit(candidate) {

    this.candidate = candidate;

    let name = candidate.user['firstName'];
    this.messages.clear();
    this.messages.add({
      key: 'e',
      sticky: true,
      severity: 'warn',
      summary: 'Are you sure want to Edit ' + '\xa0' + name + '?',
      detail: 'Confirm to proceed'
    });
    return;
  }

  onConfirm() {
   

    this.messages.clear('c');
    this.deleteAdmin(this.candidate);

  }

  onReject() {
    this.messages.clear('c');
  }

  onConfirmedit() {

    this.messages.clear('e');
    this.editadmin(this.candidate);

  }

  onRejectedit() {
    this.messages.clear('e');
  }

  clear() {
    this.messages.clear();
  }


}
