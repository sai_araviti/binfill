import { AdminService } from './admin.service';
import { LoginModule } from './../login/login.module';
import { CoreModule } from './../core/core.module';
import { ThemeComponentsModule } from './../@theme/components/components.modules';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminViewComponent } from './admin-view/admin-view.component';
import { AdminEditComponent } from './admin-view/admin-edit/admin-edit.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ThemeComponentsModule,
    CoreModule,
    LoginModule,
    FormsModule
  ],
  declarations: [AdminViewComponent, AdminEditComponent],
  exports:[AdminViewComponent],
  providers:[AdminService]
})
export class AdminModule { }
