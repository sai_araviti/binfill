import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThemeComponentsModule } from './../@theme/components/components.modules';
import { CoreModule } from './../core/core.module';
import { LoginComponent } from './login.component';
import { AuthGuard } from './auth/auth.guard';
import { NgModule,ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { PasswordresetComponent } from './passwordreset/passwordreset.component';


@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    ThemeComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    
  ],
  declarations: [
    LoginComponent,
    ForgotPasswordComponent,
    PasswordresetComponent,  
  ]
})
export class LoginModule { 

  public static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: LoginModule,
      providers: [ AuthGuard],
    };

  }
}
