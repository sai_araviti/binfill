
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import {LoginService} from './../login.service';

import {MessageService} from 'primeng/api';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
  providers: [MessageService]
})
export class ForgotPasswordComponent implements OnInit {

  constructor( private route: ActivatedRoute,
    private router: Router,
    private LoginServices: LoginService,
    private messages: MessageService) { }
  email: string ;
  ngOnInit() {

    
  }

  forget() {
   
   
    // stop here if form is invalid
      
      let _self = this;
    this.LoginServices.forgetPassword(this.email)
      .pipe(first())
      .subscribe(
        data => {
          if(data.message){
            this.router.navigate(['/login']);
            
          }
        
        },
        error => {

          _self.messages.add({ key: 'forget', severity: 'error', summary: 'Email Not Found ', detail: 'Error' + 

          '' })
         
        })
      
  }
}
