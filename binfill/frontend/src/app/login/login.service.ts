import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';

const jwt = window.localStorage.getItem('currentUser');
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
// var obj = {};
//   obj=JSON.parse(jwt)
//   let token = obj["token"];
// const httpOptions2 = {
//     headers: new HttpHeaders({'Content-Type': 'application/json',
//     Authorization: `Bearer ${jwt}`})
//   };

@Injectable({
  providedIn: 'root'
})

export class LoginService {


  constructor(private http: HttpClient) {
  }

  login(email: string, password: string) {
    return this.http.post<any>(`${environment.apiUrl}/users/api-token-auth/`,
      {email: email, password: password}, httpOptions)
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      }));
  }

  forgetPassword(email: string) {

    return this.http.post<any>(`${environment.apiUrl}/users/forgot-password-request/`, {email: email}, httpOptions)
      .pipe(map
      (email => {
          
          return email;
        }
      ));


  }


  passwordreset(password1: string, Token: any, pk: any) {
    return this.http.post<any>(`${environment.apiUrl}/users/change-password`, {password: password1, Token: Token, pk: pk})
      .pipe(map
      (password => {
          
          return password;
        }
      ));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');

  }

  //restet token
  tokenreset(token: string) {

    return this.http.post<any>(`${environment.apiUrl}/api-token-refresh/`, {token: token}, httpOptions)
      .pipe(map
      (token => {
         
          return token;
        }
      ));


  }
}
