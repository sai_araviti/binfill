import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {LoginService} from '../login/login.service';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
   providers: [MessageService]
})
export class LoginComponent implements OnInit {
  checked = false;

  loading = false;
  submitted = false;
  returnUrl: string;

  email: string;
  password: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private loginService: LoginService,
    private messages: MessageService
    ) {
    
  }

  ngOnInit() {

    this.loginService.logout();
    
    this.email = '';

    this.password = '';


     // get return url from route parameters or default to '/'
     this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/login';


  }

  onClickMe() {
    this.submitted = true;
    let _self = this;
    this.loading = true;

    this.loginService.login(this.email, this.password)
      .pipe(first())
      .subscribe(
        data => {
         
            this.router.navigate(['/base/admin']);
          
        

        },
        error => {
          
          
        
           
            _self.messages.add({ key: 'tst', severity: 'error', summary: 'Authentication Error', detail: 'Email or password not match' +
            '' })

           
            
          

          this.loading = false;
        });
  }

  forgot() {

    this.router.navigate(['/forget']);


  }
}




