import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import {LoginService} from '../login.service';
import {MessageService} from 'primeng/api';
@Component({
  selector: 'app-passwordreset',
  templateUrl: './passwordreset.component.html',
  styleUrls: ['./passwordreset.component.css'],
  providers: [MessageService]
})
export class PasswordresetComponent implements OnInit {
  Password :string;
  Passwordretype:string;
  token:any;
  pk:any;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private LoginServices: LoginService, private messages: MessageService) { }

  ngOnInit() {
    this.route
    .queryParams
    .subscribe(params => {
      
      this.token =params.token;
      this.pk=params.pk;
      
        // here you can access params['user'], params['token'], etc.
    });
    
  }
  
  reset(){
    if(this.Password === this.Passwordretype){
    
    let _self = this;
    this.LoginServices.passwordreset( this.Password,this.token,this.pk)
    .pipe(first())
    .subscribe(
      
      data => {
        _self.messages.add({ key: 'success', severity: 'success', summary: 'Success', detail: 'Password Changed Sucessfully' +
        '' })
          this.router.navigate(['/login']);
        
        
      },
      error => {
        _self.messages.add({ key: 'passmath', severity: 'error', summary: 'Authentication Error', detail: 'Password not changed' +
        '' })
              this.Password="";
              this.Passwordretype="";
      });
   

  }
}
}
