import { BaseComponent } from './../../../base/base/base.component';
import {Component} from '@angular/core';



@Component({
    selector: 'app-topbar',
    template: `
        <div class="topbar clearfix">
            <div class="topbar-left">
                <div class="logo"></div>
            </div>

            <div class="topbar-right">
                <a id="menu-button" href="#" (click)="app.onMenuButtonClick($event)">
                    <i></i>
                </a>

               

                <a id="topbar-menu-button" href="#" (click)="app.onTopbarMenuButtonClick($event)">
                    <i class="material-icons">menu</i>
                </a>

            </div>
        </div>
    `
})
export class AppTopbarComponent {

    constructor(public app: BaseComponent) {}

}
