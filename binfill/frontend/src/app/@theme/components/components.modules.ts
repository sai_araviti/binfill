import {NgModule, ModuleWithProviders} from '@angular/core';
import {AppRightpanelComponent} from './rightpanel/app.rightpanel.component';
import {
  AccordionModule,
  AutoCompleteModule,
  BreadcrumbModule,
  ButtonModule,
  CalendarModule,
  CardModule,
  CarouselModule,
  ChartModule,
  CheckboxModule,
  ChipsModule,
  CodeHighlighterModule,
  ColorPickerModule,
  ConfirmDialogModule,
  ContextMenuModule,
  DialogModule,
  DropdownModule,
  EditorModule,
  FieldsetModule,
  FileUploadModule,
  GalleriaModule,
  GrowlModule,
  InplaceModule,
  InputMaskModule,
  InputSwitchModule,
  InputTextareaModule,
  InputTextModule,
  LightboxModule,
  ListboxModule,
  MegaMenuModule,
  MenubarModule,
  MenuModule,
  MessageModule,
  MessagesModule,
  MultiSelectModule,
  OrderListModule,
  OrganizationChartModule,
  OverlayPanelModule,
  PaginatorModule,
  PanelMenuModule,
  PanelModule,
  PasswordModule,
  PickListModule,
  ProgressBarModule,
  RadioButtonModule,
  RatingModule,
  ScheduleModule,
  ScrollPanelModule,
  SelectButtonModule,
  SlideMenuModule,
  SliderModule,
  SpinnerModule,
  SplitButtonModule,
  StepsModule,
  TabMenuModule,
  TabViewModule,
  TerminalModule,
  TieredMenuModule,
  ToggleButtonModule,
  ToolbarModule,
  TooltipModule,
  TreeModule,
  TreeTableModule
} from 'primeng/primeng';
import {CommonModule} from '@angular/common';
import {AppFooterComponent} from './footer/app.footer.component';
import {AppInlineProfileComponent} from './profile/app.profile.component';
import {AppBreadcrumbComponent} from './breadcrumb/app.breadcrumb.component';
import {RouterModule} from '@angular/router';
import {AppMenuComponent, AppSubMenuComponent} from './menu/app.menu.component';
import {DataViewModule} from 'primeng/dataview';
import {TableModule} from 'primeng/table';
import {ToastModule} from 'primeng/toast';
import {AppTopbarComponent} from './topbar/app.topbar.component';
import {BreadcrumbService} from './breadcrumb/breadcrumb.service';
import {DataTableModule } from 'primeng/primeng';


const UTLIMA_THEME_MODULES = [
  AccordionModule,
  AutoCompleteModule,
  BreadcrumbModule,
  ButtonModule,
  CalendarModule,
  CardModule,
  CarouselModule,
  ChartModule,
  CheckboxModule,
  ChipsModule,
  CodeHighlighterModule,
  ConfirmDialogModule,
  ColorPickerModule,
  ContextMenuModule,
  DataTableModule,
  DataViewModule,
  DialogModule,
  DropdownModule,
  EditorModule,
  FieldsetModule,
  FileUploadModule,
  GalleriaModule,
  GrowlModule,
  InplaceModule,
  InputMaskModule,
  InputSwitchModule,
  InputTextModule,
  InputTextareaModule,
  LightboxModule,
  ListboxModule,
  MegaMenuModule,
  MenuModule,
  MenubarModule,
  MessageModule,
  MessagesModule,
  MultiSelectModule,
  OrderListModule,
  OrganizationChartModule,
  OverlayPanelModule,
  PaginatorModule,
  PanelModule,
  PanelMenuModule,
  PasswordModule,
  PickListModule,
  ProgressBarModule,
  RadioButtonModule,
  RatingModule,
  ScheduleModule,
  ScrollPanelModule,
  SelectButtonModule,
  SlideMenuModule,
  SliderModule,
  SpinnerModule,
  SplitButtonModule,
  StepsModule,
  TableModule,
  TabMenuModule,
  TabViewModule,
  TerminalModule,
  TieredMenuModule,
  ToastModule,
  ToggleButtonModule,
  ToolbarModule,
  TooltipModule,
  TreeModule,
  TreeTableModule,
];


const BINFILL_THEME_COMPONENTS = [
  AppSubMenuComponent,
  AppRightpanelComponent,
  AppFooterComponent,
  AppTopbarComponent,
  AppInlineProfileComponent,
  AppBreadcrumbComponent,
  AppMenuComponent
];

@NgModule({
  declarations: [
    ...BINFILL_THEME_COMPONENTS
  ],
  imports: [
    RouterModule,
    CommonModule,
    ScrollPanelModule,
    MenuModule,

  ],
  exports: [...BINFILL_THEME_COMPONENTS,
    ...UTLIMA_THEME_MODULES
  ]
})
export class ThemeComponentsModule {
  public static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: ThemeComponentsModule,
      providers: [BreadcrumbService],
    };
  }

}
