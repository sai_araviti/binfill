import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import { first } from 'rxjs/operators';
import {LoginService} from '../../login/login.service';
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private LoginServices: LoginService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if (err.status === 401) {
        // auto logout if 401 response returned from api
        this.LoginServices.logout();
        location.reload(true);
      }
      if(err.status ===403){
       
        let token =  currentUser.token
        this.LoginServices.tokenreset(token)
        .pipe(first())
        .subscribe(
          data => {
           
              
            
          },
          error => {
  
            
           
          })
        
        
      }
      const error = err.error.message;
      return throwError(error);
    }));
  }
}
