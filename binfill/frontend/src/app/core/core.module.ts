import { SharedService } from './services/shared.service';
import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MessageModule} from 'primeng/message';
import {DropdownModule, MessagesModule} from 'primeng/primeng';
import {ToastModule} from 'primeng/toast';
import {ProgressSpinnerModule} from 'primeng/progressspinner';

import {CountrySelectorComponent} from './components/country-selector/country-selector.component';
import {ApiService} from './services/api.services';

import {FormsModule} from '@angular/forms';
import { ProvinceSelectorComponent } from './components/country-selector/province-selector/province-selector.component';
import { CitySelectorComponent } from './components/country-selector/province-selector/city-selector/city-selector.component';


const COMPONENTS = [
  CountrySelectorComponent,
  ProvinceSelectorComponent,
  CitySelectorComponent
];

const EXPORT_MODULES = [
  MessageModule,
  MessagesModule,
  ToastModule,
  ProgressSpinnerModule
];


@NgModule({
  imports: [
    CommonModule,
    DropdownModule,
    FormsModule,
    ...EXPORT_MODULES
  ],
  exports: [
    ...EXPORT_MODULES,
    ...COMPONENTS,
  ],
  declarations: [
    ...COMPONENTS,
    
  ],

  providers: [
    ApiService,SharedService
  ]
})
export class CoreModule {
  public static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: CoreModule,

    };
  }
}
