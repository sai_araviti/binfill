

import { Component,EventEmitter, OnInit, Output, Input } from '@angular/core';
import {ApiService} from '../../../services/api.services';
import {MenuItem, SelectItem} from 'primeng/api';



class Province {
  name: string;
  id: string;
}
@Component({
  selector: 'province-selector',
  templateUrl: './province-selector.component.html',
  styleUrls: ['./province-selector.component.css']
})
export class ProvinceSelectorComponent implements OnInit {
  provinces: SelectItem[];
  selector: SelectItem;
  province: Province;
  apiService: ApiService;
 
  
  @Output() provinceSelected = new EventEmitter();
  @Input() selectedCountry ;


  constructor(apiService: ApiService) {
    this.selectedCountry 
    this.apiService = apiService;
    
   }

  ngOnInit() {

    
    
   
  }
  onChange(selectedProvince: Province) {
    
    
    this.apiService.get('/utils/province?limit=250?',).pipe().subscribe(provinceList => {
      this.provinces = provinceList.results;
    });
    this.provinceSelected.emit(selectedProvince);
  }

}
