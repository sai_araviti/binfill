import { Component,EventEmitter, OnInit, Output } from '@angular/core';
import {ApiService} from '../../../../services/api.services';
import {MenuItem, SelectItem} from 'primeng/api';


class City {
  name: string;
  id: string;
}
@Component({
  selector: 'city-selector',
  templateUrl: './city-selector.component.html',
  styleUrls: ['./city-selector.component.css']
})
export class CitySelectorComponent implements OnInit {
  cities: SelectItem[];
  selector: SelectItem;
  city: City;
  apiService: ApiService;
  @Output() citySelected = new EventEmitter();

  constructor(apiService: ApiService) {
    this.apiService = apiService;
   }

  ngOnInit() {
    this.apiService.get('/utils/country?limit=250').pipe().subscribe(cityList => {
      this.cities = cityList.results;
    });
  }
  onChange(selectedCity: City) {
    this.citySelected.emit(selectedCity);
  }

}
