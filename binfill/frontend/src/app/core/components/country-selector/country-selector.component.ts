
import { Component, EventEmitter, OnInit, Output, Input, ViewChild } from '@angular/core';
import {ApiService} from '../../services/api.services';
import {MenuItem, SelectItem} from 'primeng/api';
import { ProvinceSelectorComponent } from './province-selector/province-selector.component';

class Country {
  name: string;
  id: string;
}

@Component({
  selector: 'country-selector',
  templateUrl: './country-selector.component.html',
  styleUrls: ['./country-selector.component.css']
})
export class CountrySelectorComponent implements OnInit {

@ViewChild (ProvinceSelectorComponent)
  countries: SelectItem[];
  selector: SelectItem;
  country: Country;
  apiService: ApiService;
  @Output() countrySelected = new EventEmitter();
   
  constructor(apiService: ApiService) {
    this.apiService = apiService;

  }

  ngOnInit() {
    this.apiService.get('/utils/country?limit=250').pipe().subscribe(countryList => {
      this.countries = countryList.results;
    });
  }

  onChange(selectedCountry: Country) {
    this.countrySelected.emit(selectedCountry);

    
   
  }

}
