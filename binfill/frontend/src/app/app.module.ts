

import { BaseComponent } from './base/base/base.component';
import { RouterModule, Router } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule ,HTTP_INTERCEPTORS} from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { AppComponent } from './app.component';

import {JwtInterceptor} from './core/interceptors/jwt';
import {ErrorInterceptor} from './core/interceptors/error';
import {CoreModule} from './core/core.module';
import { ThemeComponentsModule } from './@theme/components/components.modules';
import {AuthGuard} from './login/auth/auth.guard';
import { BaseModule } from './base/base.module';
import { LoginModule } from './login/login.module';
import { AdminModule } from './admin/admin.module';
import { routes } from './app.routes';
import { LoginService } from './login/login.service';



@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        BaseModule,
        ReactiveFormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        CoreModule.forRoot(),
        ThemeComponentsModule.forRoot(),
        LoginModule,
        AdminModule,
       RouterModule.forRoot(routes)
    ],
    declarations: [
       AppComponent,
    
    ],
    providers: [ 
      AuthGuard,LoginService,

      { provide: LocationStrategy, useClass: HashLocationStrategy },
      { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        
    


  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
