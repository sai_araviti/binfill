import { CountrySelectorComponent } from './../core/components/country-selector/country-selector.component';
import { FormsModule } from '@angular/forms';
import { MessageService } from 'primeng/api';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseComponent } from './base/base.component';
import { ThemeComponentsModule } from '../@theme/components/components.modules';
import { CoreModule } from '../core/core.module';
import { AdminModule } from '../admin/admin.module';

@NgModule({
  imports: [
    CommonModule,
    ThemeComponentsModule,
    CoreModule,
    AdminModule,
    FormsModule
    
    
  ],
  declarations: [BaseComponent],

  exports:[CoreModule],
  providers:[MessageService]
})
export class BaseModule { }
