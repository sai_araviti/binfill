import { AdminViewComponent } from './admin/admin-view/admin-view.component';
import { AuthGuard } from './login/auth/auth.guard';
import { PasswordresetComponent } from './login/passwordreset/passwordreset.component';
import { ForgotPasswordComponent } from './login/forgot-password/forgot-password.component';
import { LoginComponent } from './login/login.component';
import { BaseComponent } from './base/base/base.component';

import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';



export const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
      },
    {
      path:'base',
      component: BaseComponent,canActivate:[AuthGuard],
      
      children:[
        {path:'admin',component:AdminViewComponent},
        
      ]
  
    },
      {
       path: 'forget',
        component: ForgotPasswordComponent
       },
       {
        path: 'reset-password-request',
        component: PasswordresetComponent,
      },
      // otherwise redirect to home
      { path: '**', redirectTo: 'login' }
  ];
  

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FeatureRoutingModule {}
