"""
User module urls - starts with 'users'
"""
# from users import views
from django.conf.urls import include, url
from rest_framework_jwt.views import obtain_jwt_token

from users.views import forgot_password
from rest_framework_nested import routers
from utils import views
from django.urls import path, include

from .views import admin
from .views import drivers

router = routers.SimpleRouter(trailing_slash=False)


router.register('admin', admin.AdminViewSet, 'admin')
router.register('license-type', drivers.LicenseTypeViewSet, 'license-type')
router.register('drivers', drivers.DriversViewSet, 'drivers')



urlpatterns = [
    
    url(r'^', include(router.urls)),
    url(r'api-token-auth/$', obtain_jwt_token),
    url(r'forgot-password-request/$', forgot_password.ForgotPasswordEmail.as_view()),
    url(r'change-password$', forgot_password.PasswordResetConfirmed.as_view()),
] 

