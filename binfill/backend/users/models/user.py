from datetime import timedelta

from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from utils.shortcuts import generate_random_number


class BinfillUserManager(BaseUserManager):
    """
    A custom user manager to deal with emails as unique identifiers for auth
    instead of usernames. The default that's used is "UserManager"
    """

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The Email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        user = self._create_user(email, password, **extra_fields)
        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True, null=True)

    first_name = models.CharField(verbose_name=_('First name'), max_length=128)
    last_name = models.CharField(verbose_name=_('Last name'), max_length=128)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ), )
    is_valid = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should is active. '
        ),
    )
    USERNAME_FIELD = 'email'
    objects = BinfillUserManager()

    def __str__(self):
        return self.email

    def get_full_name(self):
        return ' '.join(filter(None, [self.first_name, self.last_name]))

    def get_short_name(self):
        return self.email


class ResetPasswordToken(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=10)
    expiry = models.DateTimeField()

    def __str__(self):
        return format("Reset token for {self.user}")

    @classmethod
    def get_or_genreate_token(cls, user):
        """
        Try to get a reset token for the given user,
        If the token expired or doesnt exists, creates a new one
        """
        token = cls.objects.filter(user=user, expiry__gte=timezone.now()).first()
        if not token:
            token = cls.objects.create(
                user=user,
                token=generate_random_number(8),
                expiry=timezone.now() + timedelta(days=1)
            )
        return token.token

    @classmethod
    def is_valid(cls, token, pk):
        """
        returns a  token object, if the token is valid for the user
        """
        return cls.objects.filter(user__id=pk, expiry__gte=timezone.now(), token=token).first()
