# Company drivers models
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from company.models import company_details, trucks
from users.models import User


class LicenseType(models.Model):
    """ Model class type for license type  """

    license_type = models.CharField(max_length=20, null=True)

    def __unicode__(self):
        return self.id


class Driver(models.Model):
    """Model class for drivers """

    user = models.OneToOneField(User, on_delete=models.CASCADE, blank=True)
    phone = PhoneNumberField()
    company = models.ForeignKey(company_details.Company, db_index=True, on_delete=models.CASCADE,
                                null=True, blank=True)
    display_id = models.CharField(max_length=256, null=True, blank=True)
    license_number = models.CharField(max_length=256)
    expiry_date = models.DateField(auto_now=False)
    license_type = models.ForeignKey(LicenseType, on_delete=models.CASCADE)
    truck = models.OneToOneField(trucks.Truck, on_delete=models.PROTECT, null=True, blank=True)
    license = models.FileField(null=True, upload_to='drivers/license/')
    experience = models.FloatField(default=0, null=True)
    is_deleted = models.BooleanField(default=False)
    deleted_on = models.DateTimeField(auto_now_add=False, null=True)
    created_by = models.ForeignKey(User, db_index=True, on_delete=models.PROTECT,
                                   related_name='driver_createdby', null=True, blank=True)

    def __unicode__(self):
        return self.id
