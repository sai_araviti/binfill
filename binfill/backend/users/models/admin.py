# Admin models
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from users.models import User
from utils.models.mixins import AddressMixin


class AdminUser(AddressMixin):
    """Admin Users model """

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = PhoneNumberField(blank=True)
    display_id = models.CharField(max_length=128, null=True, unique=True)

    created_by = models.ForeignKey(User, db_index=True, on_delete=models.CASCADE,
                                   related_name='admin_createdby', null=True, blank=True)
    edited_by = models.ForeignKey(User, db_index=True, on_delete=models.CASCADE,
                                  related_name='admin_editedby', null=True, blank=True)
    deleted_on = models.DateTimeField(auto_now=False, null=True)

    def __unicode__(self):
        return self.id
