import django_filters
from users.models import drivers

class DriversFilterSet(django_filters.rest_framework.FilterSet):

    """Filter class for trucks module"""
    id = django_filters.NumberFilter(field_name="id")
    phone = django_filters.NumberFilter(field_name="phone")
    license_number = django_filters.CharFilter(field_name="license_number")
    license_type = django_filters.CharFilter(field_name="license_type")
    experience = django_filters.CharFilter(field_name="experience")

    class Meta:
        model = drivers.Driver
        fields = ['id', 'phone', 'license_number', 'license_type', 'experience']
