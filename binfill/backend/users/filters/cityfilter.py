import django_filters
from cities import models as city_models

class CountryFilterSet(django_filters.rest_framework.FilterSet):

    id = django_filters.NumberFilter(field_name="id")
    name = django_filters.CharFilter(field_name="name")
    class Meta:
         model = city_models.Country
         fields = ['id','name']

class ProvinceFilterSet(django_filters.rest_framework.FilterSet):
    id =django_filters.NumberFilter(field_name="id")
    name = django_filters.CharFilter(field_name="name")
    country_id = django_filters.NumberFilter(field_name="country_id", )
    country_name = django_filters.CharFilter(field_name="country_name")

    class Meta:
         model = city_models.Region
         fields = ['id','name','country_id','country_name']


class CityFilterSet(django_filters.rest_framework.FilterSet):
    id = django_filters.NumberFilter(field_name="id")
    name = django_filters.CharFilter(field_name="name", lookup_expr='icontains')
    country_id = django_filters.NumberFilter(field_name="country_id", )
    country_name = django_filters.CharFilter(field_name="country_name" )
    province_id = django_filters.NumberFilter(field_name="region_id" )
    province_name = django_filters.CharFilter(field_name="region_name" )

    class Meta:
         model = city_models.City
         fields = ['id','name','country_id','country_name','province_id','province_name']
