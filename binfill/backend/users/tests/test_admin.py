from rest_framework.test import APITestCase
from django.test import Client
from users.models import User

class TestAdmin(APITestCase):

    """Test the admin module"""

    fixtures = ['users.json', 'city.json']

    data = {"user": {
                "first_name": "test",
                "last_name": "user",
                "email": "testadmin@gmail.com"
            },

            "street": "street",
            "postalCode": "68945454",
            "phoneNumber": "+919894444474",
            "country": 1,
            "province": 1,
            "city": 1,
            }


    def setUp(self):
        self.user = User.objects.get(pk=1)
        self.client = Client()
        self.client.login(email='adminuser@gmail.com', password='binfill@1234')

    def test_admin(self):

        """
        Get,post,put,delete admin users..API should return 201 or 200 response
        """
        response = self.client.post('/users/admin', self.data, content_type='application/json')
        self.assertEqual(response.status_code, 201, "API should return 200 OK status")
        response = self.client.get('/users/admin', self.data, content_type='application/json')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")
        response = self.client.get('/users/admin/1', self.data, content_type='application/json')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")
        self.data['street'] = "street1"
        self.data['user']['last_name'] = "testadmin"
        response = self.client.put('/users/admin/1', self.data, content_type='application/json')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")
        response = self.client.delete('/users/admin/1', self.data, content_type='application/json')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")


    def test_admin_email_exist(self):
        import pdb
        pdb.set_trace()
        self.data['user']['email'] = "adminuser@gmail.com"
        response = self.client.post('/users/admin', self.data, content_type='application/json')
        self.assertEqual(response.status_code, 400, "API should return 400 OK status")
