from rest_framework.test import APITestCase
from django.test import Client

class TestAuthentication(APITestCase):
    """Test the authentication"""

    fixtures = ["users.json"]

    data = {"email": " adminuser@gmail.com","password": "binfill@1234"}

    def setUp(self):
        self.client = Client()

    def valid_login(self):
        """
        Login with a valid username and password, make sure that API returns success

        """
        response = self.client.post('/api-token-auth/', self.data, content_type='application/json')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")
        self.assertEqual(response.data['user']['user_role'], '')


    def login_invalid_password(self):
        """
        Login with an valid username and wrong password, make sure that API returns unauthorized

        """
        self.data['password'] = "Swordfish"
        response = self.client.post('/api-token-auth/',self.data,
                                    content_type='application/json')
        self.assertEqual(response.status_code, 403, "API should \
                                                    return 403 status if password is wrong")


    def login_invalid_username(self):
        """
        Login with an invalid username and a password, make sure that API returns unauthorized

        """
        self.data['email'] = "admin@gmail.com"
        response = self.client.post('/api-token-auth/', self.data, content_type='application/json')
        self.assertEqual(response.status_code, 403, "API should return 403 status if username is wrong")

    def test_authenticate(self):
        self.valid_login()
        self.login_invalid_password()
        self.login_invalid_username()

