from rest_framework.test import APITestCase
from django.core.files.uploadedfile import SimpleUploadedFile

from django.test import Client
from users.models import User
import json

class TestDriver(APITestCase):

    """Test the driver module"""

    fixtures = ['users.json', 'city.json', 'company.json', 'truck.json','driver.json', "license_type.json"]

    # license = SimpleUploadedFile("/home/user/Desktop/mapdoc.odt", b"file_content", content_type="odt")
    license = SimpleUploadedFile(name ="download.jpeg", content=open("C:\\Users\\cy0106\\Downloads\\download.jpeg","rb").read())
    headers = {'Content-type': None}

    data = {
            "email": "testdriver@gmail.com",
            "first_name": "test",
            "last_name": "driver",
            "company":1,
            "experience": "2.1",
            "truck": 2,
            "license_type": 1,
            "expiry_date": "2019-01-01",
            "phone": "+917025186432",
            "license_number": "34546546546",
            "license":license
            }


    def setUp(self):
        self.user = User.objects.get(pk=1)
        self.client = Client()
        self.client.login(email='supercoordinator@outlook.com', password='binfill@1234')

    def drivers(self):
        """
        Get,post,put,delete drivers..API should return 201 or 200 response
        """
        response = self.client.post('/users/drivers', self.data)
        self.assertEqual(response.status_code, 201, "API should return 200 OK status")
        response = self.client.get('/users/drivers')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")
        response = self.client.get('/users/drivers/1')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")
        response = self.client.delete('/users/drivers/1')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")


    def driver_email_exist(self):
        self.data['email'] = "stephy@gmail.com"
        response = self.client.post('/users/drivers', self.data)
        self.assertEqual(response.status_code, 400, "API should return 400 OK status")

    def test_drivers(self):
        self.drivers()
        self.driver_email_exist()
