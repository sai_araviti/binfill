from rest_framework.test import APITestCase
from django.test import Client
from users.models import User

class TestLicensetype(APITestCase):

    """Test the driver module"""

    fixtures = ['users.json']

    data = {
            "license_type": "G1",
            }


    def setUp(self):
        self.user = User.objects.get(pk=1)
        self.client = Client()
        self.client.login(email='supercoordinator@outlook.com', password='binfill@1234')

    def test_licensetype(self):

        """
        Get,post license type ..API should return 201 or 200 response

        """
        response = self.client.post('/users/license-type', self.data, content_type='application/json')
        self.assertEqual(response.status_code, 201, "API should return 200 OK status")
        response = self.client.get('/users/license-type', content_type='application/json')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")

