import datetime

import pytz
from django.contrib.auth.hashers import make_password
from django.template.loader import render_to_string
from django.utils import timezone
from rest_framework import serializers

from company.serializers import company_serializers,trucks
from users.models import drivers, user
from users.serializers.admin import UserSerializer
from utils.email import send_email
from utils.generate_password import RandomPasswordgenerator


class LicenseTypeSerializer(serializers.ModelSerializer):
    """ License type serializer"""

    class Meta:
        model = drivers.LicenseType
        fields = ('__all__')


class DriversSerializer(serializers.ModelSerializer):

    def validate(self, data):

        expiry_date = datetime.datetime.strptime(str(data['expiry_date']), "%Y-%m-%d")
        if pytz.utc.localize(expiry_date) < timezone.now():
            raise serializers.ValidationError("Provide valid expiry date")
        request = self.context.get('request')
        method = request.META['REQUEST_METHOD']
        if method == 'POST':
            if user.User.objects.filter(email=request.data.get('email')):
                raise serializers.ValidationError({'user': {'email': ['User with this email id already exists.']}})
        elif method == 'PUT':
            if user.User.objects.filter(email=request.data.get('email')).exclude(pk=self.instance.user.id):
                raise serializers.ValidationError({'user': {'email': ['User with this email id already exists.']}})
        return data

    def create(self, validated_data):

        request = self.context.get('request')
        user_data = {"first_name": request.data.get('first_name'),
                     "last_name": request.data.get('last_name'),
                     "email": request.data.get('email')}
        password = RandomPasswordgenerator.password_generator(self)
        set_password = make_password(password)
        user_data['password'] = set_password
        serializer = UserSerializer(data=user_data)
        serializer.is_valid(raise_exception=True)
        user = UserSerializer.create(UserSerializer(), validated_data=user_data)
        driver_entry = drivers.Driver.objects.all().order_by('id').last()
        if driver_entry is None:
            validated_data['display_id'] = "DR" + "01"

        else:
            display_id = int(driver_entry.display_id[2:]) + 1
            validated_data['display_id'] = "DR" + str("%02d" % display_id)
        validated_data['created_by_id'] = self.context['request'].user.id
        new_driver = drivers.Driver.objects.create(user=user, **validated_data)
        ctx = {
            'email': user.email,
            'password': password
        }
        html_content = render_to_string('email_on_create_admin.html', ctx)
        send_email("Registered Successfully", html_content, [user.email])
        return new_driver

    def update(self, instance, validated_data):
        request = self.context.get('request')
        user_data = {"first_name": request.data.get('first_name'),
                     "last_name": request.data.get('last_name'),
                     "email": request.data.get('email')}
        user.User.objects.filter(pk=instance.user.id).update(**user_data)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.company = validated_data.get('company', instance.company)
        instance.license_number = validated_data.get('license_number', instance.license_number)
        instance.expiry_date = validated_data.get('expiry_date', instance.expiry_date)
        instance.license_type = validated_data.get('license_type', instance.license_type)
        instance.truck = validated_data.get('truck', instance.truck)
        instance.license = validated_data.get('license', instance.license)
        instance.experience = validated_data.get('experience', instance.experience)
        instance.save()
        return instance

    class Meta:
        model = drivers.Driver
        fields = ('__all__')


class DriverListSerializer(serializers.ModelSerializer):
    truck = trucks.TrucksSerializer()
    company = company_serializers.CompanySerializer()
    user = UserSerializer()

    class Meta:
        model = drivers.Driver
        fields = ('__all__')
