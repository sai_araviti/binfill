from django.contrib.auth.hashers import make_password
from django.template.loader import render_to_string
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from users.models import User, AdminUser
from utils.email import send_email
from utils.generate_password import RandomPasswordgenerator
from utils.serializers.cities import CitySerializer, ProvinceSerializer, CountrySerializer


class UserSerializer(serializers.ModelSerializer):
    """User Serializer"""
    email = serializers.EmailField(
        validators=[]
    )

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email')

class AdminSerializer(serializers.ModelSerializer):
    """Admin Serializer for creating and updating admins """
    user = UserSerializer(required=True)

    def validate(self, data):

        request = self.context.get('request')
        method = request.META['REQUEST_METHOD']
        if method =='POST':
            if User.objects.filter(email=data['user']['email']):
                raise serializers.ValidationError({'user': {'email': ['User with this email id already exists.']}})
        elif method =='PUT':
            if User.objects.filter(email=data['user']['email']).exclude(pk=self.instance.user.id):
                raise serializers.ValidationError({'user': {'email': ['User with this email id already exists.']}})
        return data


    def create(self, validated_data):
        user_data = validated_data.pop('user')
        password = RandomPasswordgenerator.password_generator(self)
        set_password = make_password(password)
        user_data['password'] = set_password
        user = UserSerializer.create(UserSerializer(), validated_data=user_data)
        validated_data['created_by_id'] = self.context['request'].user.id
        validated_data['display_id'] = self._generate_display_id()
        new_user = AdminUser.objects.create(user=user, **validated_data)

        ctx = {
            'email': user.email,
            'password': password
        }
        html_content = render_to_string('email_on_create_admin.html', ctx)
        send_email("Registered Successfully", html_content, [user.email])
        return new_user

    def update(self, instance, validated_data):
        user_data = validated_data.pop('user')
        instance.user.first_name = user_data.get('first_name', instance.user.first_name)
        instance.user.last_name = user_data.get('last_name', instance.user.last_name)
        instance.user.email = user_data.get('email', instance.user.email)
        instance.user.save()

        instance.phone_number = validated_data.get('phone_number', instance.phone_number)
        instance.postal_code = validated_data.get('postal_code', instance.postal_code)
        instance.country = validated_data.get('country', instance.country)
        instance.province = validated_data.get('province', instance.province)
        instance.street = validated_data.get('street', instance.street)
        instance.city = validated_data.get('city', instance.city)
        instance.edited_by_id = self.context['request'].user.id
        instance.save()
        return instance

    def _generate_display_id(self):
        last_admin_entry = AdminUser.objects.all().order_by('id').last()
        if last_admin_entry:
            display_id = "BA{0:0=3d}".format(
                int("".join(filter(lambda x: x.isdigit(), last_admin_entry.display_id))) + 1)
        else:
            display_id = "BA001"
        return display_id

    class Meta:
        model = AdminUser
        fields = ('__all__')


class AdminListSerializer(serializers.ModelSerializer):
    """ Admin List Serializer"""

    user = UserSerializer()
    country = CountrySerializer()
    province = ProvinceSerializer()
    city = CitySerializer()

    class Meta:
        model = AdminUser
        fields = ('__all__')
