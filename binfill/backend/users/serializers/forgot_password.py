from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers

from users.models.user import ResetPasswordToken


class ResetPasswordRequestSerializer(serializers.Serializer):
    email = serializers.EmailField()


class ChangePasswordSerializer(serializers.Serializer):
    # email = serializers.EmailField(help_text="Email ID of the user")
    pk = serializers.IntegerField(help_text="ID of the user")
    token = serializers.CharField(help_text="Access token from the email link")
    password = serializers.CharField(help_text="The new password to be set",
                                     validators=[validate_password]
                                     )

    def validate(self, data):
        # self.token = ResetPasswordToken.is_valid(token=data['token'], email=data['email'])
        self.token = ResetPasswordToken.is_valid(token=data['token'], pk=data['pk'])
        if not self.token:
            raise serializers.ValidationError({'token': 'This link has been expired or not valid'})
        return data

    def save(self, *args, **kwargs):
        self.token.user.set_password(self.validated_data['password'])
        self.token.user.save()
        self.token.delete()
