# Create your views here.
import django_filters
from rest_framework import viewsets, filters
from rest_framework.response import Response
from django.utils import timezone
from rest_framework.parsers import MultiPartParser, FormParser
from users.models import drivers
from users.serializers import drivers as drivers_serializers
from users.filters import driverfilter

class LicenseTypeViewSet(viewsets.ModelViewSet):

    """ License Type viewset """

    queryset = drivers.LicenseType.objects.all()
    serializer_class = drivers_serializers.LicenseTypeSerializer
    http_method_names = ['get','post']


class DriversViewSet(viewsets.ModelViewSet):

    """ Drivers viewset """
    queryset = drivers.Driver.objects.filter(is_deleted=False)
    serializer_class = drivers_serializers.DriversSerializer
    parser_classes = (MultiPartParser, FormParser)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend, filters.OrderingFilter,
                       filters.SearchFilter,)
    filterset_class = driverfilter.DriversFilterSet
    search_fields = ['license_number', 'expiry_date', 'license_type', 'experience']
    ordering_fields = '__all__'

    def get_queryset(self):

        """ overriding driver queryset """
        company_id = self.queryset.filter(created_by=self.request.user.id).values('company')
        queryset = drivers.Driver.objects.filter(company__in=company_id, is_deleted=False)
        return queryset

    def get_serializer_class(self):

        """ overriding driver serializer_class """
        if self.action == 'retrieve' or self.action == 'list':
            serializer = drivers_serializers.DriverListSerializer
        else:
            serializer = drivers_serializers.DriversSerializer
        return serializer

    def destroy(self, request, *args, **kwargs):

        """ deleting drivers """
        instance = self.get_object()
        instance.deleted_on = timezone.now()
        instance.is_deleted = True
        instance.truck = None
        instance.save()
        return Response({'message':'success'})
