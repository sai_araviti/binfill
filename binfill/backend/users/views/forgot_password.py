from django.contrib.auth import get_user_model
from django.template.loader import render_to_string
from rest_framework.generics import GenericAPIView
from drf_yasg.utils import swagger_auto_schema
from utils.email import send_email
from utils.shortcuts import success, failure, get_host, GenericSuccessSerializer
from users.models.user import ResetPasswordToken
from ..serializers.forgot_password import ResetPasswordRequestSerializer, ChangePasswordSerializer
from django.conf import settings

class ForgotPasswordEmail(GenericAPIView):
    '''
    send reset password link to mail
    '''
    serializer_class = ResetPasswordRequestSerializer
    permission_classes = ()

    @swagger_auto_schema(responses={200: GenericSuccessSerializer()})
    def post(self, request):
        print(request.data)
        serializer = self.get_serializer(data=request.data)        
        if serializer.is_valid():
            return self.try_send_password_request_link(serializer.validated_data['email'])            
        else:
            return failure(serializer.errors)

    def try_send_password_request_link(self, mail_id):
        message = "You will receive an email to reset your password shortly if {} exists in our system".format(mail_id)
        try:
            user = get_user_model().objects.get(email = mail_id)
        except get_user_model().DoesNotExist:
            print("User does not exists - no actions required")
        else:
            password_reset_link = "{url}{pk}{token}".format(
                url = settings.EMAIL_URL_LINK+"reset-password-request?pk=",
                pk = str(user.id) + "&token=",
                token = ResetPasswordToken.get_or_genreate_token(user)
            )
            ctx = {
                'username' : mail_id,
                'password_reset_link' : password_reset_link
                }
            print(password_reset_link)
            html_content = render_to_string('forgot_password_email.html', ctx)

            send_email("Binfill reset password link", html_content, [mail_id])
        return success(message=message)

class PasswordResetConfirmed(GenericAPIView):
    '''
    changes the password of a user new password
    ''' 
    serializer_class = ChangePasswordSerializer
    permission_classes = ()

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        #update new password
        if serializer.is_valid():
            serializer.save()
            return success(message="Your password has been changed succesfully")
        else:
            return failure(serializer.errors)