
from rest_framework import exceptions
from django.contrib.auth import get_user_model
from django.core.validators import validate_email
from users.models import User

class UserAuthenticationBackend(object):
    """
    Authentication Class
    """
    def authenticate(self, request, email=None, password=None):
        """
            Authenticate All users
        """
        validate_email(email)
        valid_email = True
        kwargs = {'email': email}

        try:
            user = get_user_model().objects.get(**kwargs)

        except get_user_model().DoesNotExist:
            raise exceptions.AuthenticationFailed('User does not exist.')

        if valid_email and user.check_password(password) and user.is_valid is True:

            return user

        elif valid_email and user.check_password(password) and user.is_valid is False:
            raise exceptions.AuthenticationFailed('Sorry your account is suspended temporarily,'
                                                  ' please contact Binfill for more details.')
        else:
            raise exceptions.AuthenticationFailed('Sorry, your email and password does not match.')

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
