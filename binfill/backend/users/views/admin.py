# Create your views here.
from django.utils import timezone
from rest_framework.response import Response
from rest_framework import viewsets, filters
from users.serializers import admin as admin_serializers
from users.models import user, admin


class AdminViewSet(viewsets.ModelViewSet):

    queryset = admin.AdminUser.objects.filter(deleted_on__isnull=True)
    serializer_class = admin_serializers.AdminSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('street', 'user__email', 'phone_number','display_id','user__first_name','user__last_name')
    ordering_fields = '__all__'

    def get_serializer_class(self):

        if self.action == 'retrieve' or self.action == 'list':
            serializer = admin_serializers.AdminListSerializer
        else:
            serializer = admin_serializers.AdminSerializer
        return serializer

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.deleted_on = timezone.now()
        instance.save()
        user.User.objects.filter(pk=instance.user.id).update(email=None, is_valid=False)
        return Response({'message':'sucess'})
