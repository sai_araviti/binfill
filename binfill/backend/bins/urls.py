from django.conf.urls import include, url
from rest_framework_nested import routers
from bins.views import bin_details
from rest_framework.test import APIRequestFactory

router = routers.SimpleRouter(trailing_slash=False)
router.register('bin', bin_details.BinViewSet, 'crud-bins')
router.register('bin-size', bin_details.BinSizeViewSet, 'bins-size')



"""
Company module urls - starts with 'company'

"""


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'csv$', bin_details.BinCsvDownloadViewSet.as_view()),

]
