from django.db import models
from cities.models import *
from company.models import Company
from cities.models import Country, Region, City

class BinSize(models.Model):
    '''
        model for bin_size
    '''
    size = models.IntegerField(unique=True)
    length = models.DecimalField(max_digits=5, decimal_places=2)
    width = models.DecimalField(max_digits=5, decimal_places=2)
    height = models.DecimalField(max_digits=5, decimal_places=2)

    def get_size(self):
        return self.size

    class Meta:
        unique_together = ('length', 'width', 'height')

class Bin(models.Model):
    '''
        model for create bin details
    '''
    display_id  = models.CharField(max_length=256, null=True, blank=True, unique = True)
    company = models.ForeignKey(Company, db_index=True, on_delete=models.CASCADE, null=True, blank=True)
    size = models.ForeignKey(BinSize, db_index=True, on_delete=models.PROTECT, null=True, blank=True)
    bincode = models.CharField(max_length = 25, null = False)
    street = models.CharField(max_length=256, null=True, blank=True)
    availability = models.BooleanField(default=True)
    latitude = models.DecimalField(max_digits=25, decimal_places=6, null = True, blank = True)
    longitude = models.DecimalField(max_digits=25, decimal_places=6, null = True, blank = True)
    country = models.ForeignKey(Country, db_index=True, on_delete=models.PROTECT,
                                related_name="%(app_label)s_%(class)s_related", null = True, blank = True)
    province = models.ForeignKey(Region, db_index=True, on_delete=models.PROTECT,
                                 related_name="%(app_label)s_%(class)s_related", null = True, blank = True)
    city = models.ForeignKey(City, db_index=True, on_delete=models.PROTECT,
                             related_name="%(app_label)s_%(class)s_related", null = True, blank = True)
    postal_code = models.CharField(max_length=10, null = True, blank = True)
    is_deleted = models.BooleanField(default=False)
    deleted_on = models.DateTimeField(auto_now_add=False, null=True)


    def __unicode__(self):
        return str(self.id)




