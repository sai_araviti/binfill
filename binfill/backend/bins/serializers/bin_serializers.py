from bins.models import Bin, BinSize
from rest_framework import serializers
from utils.serializers.cities import CitySerializer, CountrySerializer, ProvinceSerializer
from utils.shortcuts import get_geo_address
from cities.models import *
from django.db.models import Q


class BinSpecification:
    '''
    BinSpecifications for auto generated display id
    and address from longitude and latitude and convert it into objects
    '''
    def get_display_id(self):
        '''
        Get bin id
        '''
        bin_id = ""

        bin_record = Bin.objects.values("display_id").order_by('id').last()
        if bin_record is None:
            bin_id = "BIN001"
        else:
            bin_id = int(bin_record["display_id"][3:]) + 1
            bin_id = "BIN" + str("%03d" % bin_id)
        return bin_id

    def get_address(self, formatted_address):
        '''
         get address objects using formatted_address
        '''
        address = formatted_address.split(',')
        street = address[0]
        city = address[1]
        postal_region = address[2].strip().split(" ")
        postal_code = postal_region[1]
        province = postal_region[0]
        country = address[3]

        country_id = Country.objects.filter(Q(code=country.strip()) | Q(code3=country.strip()))[0]
        province_id = Region.objects.filter(country_id=country_id, code=province.strip())[0]
        city_id = City.objects.filter(country_id=country_id, region_id=province_id, name=city.strip())[0]

        return country_id, province_id, city_id, street,  postal_code

class BinSizeSerializer(serializers.ModelSerializer):
    '''
    bin size serializer
    '''
    class Meta:
        model = BinSize
        fields = ('__all__')

class BinSerializer(serializers.ModelSerializer):
    '''
    Bin Serialier for creating and updating bins
    '''

    def validate_bincode(self, bincode):
        company_object = self.context['request'].user.coordinator.company
        if hasattr(self.instance, 'id'):
            if Bin.objects.filter(bincode=bincode, company_id=company_object.id).exclude(id=self.instance.id).exists():
                raise serializers.ValidationError('bincode already exists for this company')
        elif Bin.objects.filter(bincode=bincode, company_id=company_object.id):
            raise serializers.ValidationError('bincode already exists for this company')
        return bincode

    def validate(self, attrs):
        """
        validate bin while creation and updation
        """
        latitude = attrs.get('latitude', "")
        longitude = attrs.get('longitude', "")
        country = attrs.get('country', "")
        province = attrs.get('province', "")
        city = attrs.get('city', "")
        street = attrs.get('street', "")
        postal_code = attrs.get('postal_code', "")

        if all(prop == "" for prop in [longitude, latitude, country, province, city, postal_code, street]):
            raise serializers.ValidationError('please provide either bin address or coordinates')
        return attrs

    def create(self, validated_data):
        '''
        create new bin
        '''
        company_object = self.context['request'].user.coordinator.company
        validated_data["company"] = company_object
        validated_data['display_id'] = BinSpecification().get_display_id()
        longitude = validated_data.get("longitude", "")
        latitude = validated_data.get("latitude", "")
        if (longitude != latitude != ""):
            formatted_address = get_geo_address(latitude, longitude)
            country, province, city, street, postal_code  = BinSpecification().get_address(formatted_address)
            validated_data['country'] = country
            validated_data['province'] = province
            validated_data['city'] = city
            validated_data['street'] = street
            validated_data['postal_code'] = postal_code


        new_bin = Bin.objects.create(**validated_data)
        return new_bin

    def update(self, instance, validated_data):
        '''
        update bin details
        '''
        latitude = validated_data.get('latitude', "")
        longitude = validated_data.get('longitude', "")
        instance.bincode = validated_data.get('bincode', instance.bincode)
        instance.size = validated_data.get('size', instance.size)
        instance.country = validated_data.get('country', instance.country)
        instance.province = validated_data.get('province', instance.province)
        instance.city = validated_data.get('city', instance.city)
        instance.street = validated_data.get('street', instance.street)
        instance.postal_code = validated_data.get('postal_code', instance.postal_code)

        if (latitude != longitude != ""):
            formatted_address = get_geo_address(latitude, longitude)
            country, province, city, street, postal_code = BinSpecification().get_address(formatted_address)
            instance.latitude = latitude
            instance.longitude = longitude
            instance.country = country
            instance.province = province
            instance.city = city
            instance.street = street
            instance.postal_code = postal_code

        instance.save()

        return instance

    class Meta:
        model = Bin
        fields = ('__all__')


class BinListSerializer(serializers.ModelSerializer):
    '''
    bins list view
    '''
    size = BinSizeSerializer()
    country = CountrySerializer()
    province = ProvinceSerializer()
    city = CitySerializer()

    class Meta:
        model = Bin
        fields = ('__all__')


