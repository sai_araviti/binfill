from bins.models import Bin, BinSize
from bins.serializers.bin_serializers import BinSerializer, BinSizeSerializer
from rest_framework import viewsets, filters
from bins.serializers import bin_serializers
import datetime
from django.http import HttpResponse
import csv
from rest_framework.generics import ListAPIView

class BinSizeViewSet(viewsets.ModelViewSet):
    '''
        bin_size view set
    '''
    queryset = BinSize.objects.all()
    serializer_class = BinSizeSerializer


class BinViewSet(viewsets.ModelViewSet):
    '''
        create and update bins
    '''
    queryset = Bin.objects.all()
    serializer_class = BinSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('binsize__height', 'binsize__length', 'binsize__width', 'binsize__size')
    ordering_fields = '__all__'

    def get_queryset(self):
        '''
        returns bins that belongs in current company
        '''
        return self.queryset.filter(company_id=self.request.user.coordinator.company)

    def get_serializer_class(self):
        """
        overriding driver serializer_class
        """
        if self.action == 'retrieve' or self.action == 'list':
            serializer = bin_serializers.BinListSerializer
        else:
            serializer = bin_serializers.BinSerializer
        return serializer

class BinCsvDownloadViewSet(ListAPIView):
    '''
        Get bins list into csv format
    '''
    def list(self, request):
        queryset = Bin.objects.all()
        file_name = "bin_%s.csv" % (datetime.datetime.now())
        response = HttpResponse(content_type='application/csv')
        response['Content-Disposition'] = 'attachment; filename=%s'%(file_name)

        writer = csv.writer(response)
        writer.writerow(['DisplayId', 'BinCode', 'BinSize', 'CurrentLocation', 'Availability'])
        field_list = [[i+1, f.display_id, f.bincode, f.size.size, str(f.country.name)+', '+str(f.province.name) \
                       +' '+str(f.postal_code)+', '+str(f.city)+','+str(f.street), f.availability]for i, f in enumerate(queryset) if f]
        writer.writerows(field_list)
        return response

