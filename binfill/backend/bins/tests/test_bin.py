from users.models import *
from users.models.user import User

from rest_framework import status
from rest_framework.test import APITestCase
import json

class BinSetUp(APITestCase):
    fixtures = ["users.json", "company.json", "city.json", "coordinator.json", "bin.json"]
    def setUp(self):
        self.user = User.objects.get(email='supercoordinator@outlook.com')
        self.client.force_authenticate(user=self.user)
        self.content_type = "application/json"

class BinTest(BinSetUp):

    def open_file(self):
        with open("bins/fixtures/create_bin.json") as f:
            create_bin = json.load(f)
        return create_bin

    def test_bin_create(self):
        # create bin api test case
        content = self.open_file()
        response = self.client.post("/bins/bin", json.dumps(content),  content_type=self.content_type)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_bin_list(self):
        # get bin list api test case
        response = self.client.get('/bins/bin/1')
        self.assertEqual(response.status_code, 200)

    def test_bin_update(self):
        # bin update api test case
        content = self.open_file()
        content["longitude"] = "-73.126743"
        response = self.client.put('/bins/bin/1', json.dumps(content), content_type=self.content_type)
        self.assertEqual(response.status_code, 200)

    def test_bin_delete(self):
        # bin delete api test case
        response = self.client.delete('/bins/bin/1')
        self.assertEqual(response.status_code, 204)

class BinSizeTest(BinSetUp):

    def open_file(self):
        with open("bins/fixtures/create_bin_size.json") as f:
            create_bin = json.load(f)
        return create_bin

    def test_bin_size_create(self):
        # create bin api test case
        content = self.open_file()
        response = self.client.post("/bins/bin-size", json.dumps(content), content_type=self.content_type)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_bin_size_list(self):
        # get bin list api test case
        response = self.client.get('/bins/bin-size/1')
        self.assertEqual(response.status_code, 200)

    def test_bin_size_update(self):
        # bin update api test case
        content = self.open_file()
        content["width"] = 1
        response = self.client.put('/bins/bin-size/1', json.dumps(content), content_type=self.content_type)
        self.assertEqual(response.status_code, 200)

class BinCSVTest(BinSetUp):

    def test_bin_csv_list(self):
        # get bin lists in csv format api test case
        response = self.client.get('/bins/csv')
        self.assertEqual(response.status_code, 200)