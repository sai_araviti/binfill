from .development import *

DATABASES['default']['NAME'] = 'binfill2'
DATABASES['default']['USER'] = 'postgres'
DATABASES['default']['PASSWORD'] = 'cycloides@123'
DATABASES['default']['TEST']['NAME'] = 'mytest'

OSGEO4W = r"C:\OSGeo4W64"
#     if '64' in platform.architecture()[0]:
#         OSGEO4W += "64"
assert os.path.isdir(OSGEO4W), "Directory does not exist: " + OSGEO4W
os.environ['OSGEO4W_ROOT'] = OSGEO4W
os.environ['GDAL_DATA'] = OSGEO4W + r"\share\gdal"
os.environ['PROJ_LIB'] = OSGEO4W + r"\share\proj"
os.environ['PATH'] = OSGEO4W + r"\bin;" + os.environ['PATH']