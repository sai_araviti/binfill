from phonenumber_field.modelfields import PhoneNumberField
from django.db.models.deletion import SET_NULL
from django.db import models
from cities.models import *
from users.models.user import User
from utils.models.mixins import AddressMixin
from transfer_stations.models import transfer_stations

class Company(AddressMixin):
    """
        model for company_details
    """

    company_size_choices = (('small', '(0-50 Bins)'),
                            ('medium', '(50-100 Bins)'),
                            ('large', '(100-200 Bins)'),
                           )

    is_deleted = models.BooleanField(default=False)
    deleted_on = models.DateTimeField(auto_now=False, null=True)
    size = models.CharField(max_length=5, choices=company_size_choices)
    created_by = models.ForeignKey(User, null=True, blank=True, on_delete=SET_NULL)
    name = models.CharField(max_length=100, unique=True)
    binfill_page = models.CharField(max_length=100, unique=True)
    transfer_stations = models.ManyToManyField(transfer_stations.TransferStation)
    email = models.EmailField(unique=True)
    phone = PhoneNumberField(blank=True)
    hst_reg = models.CharField(max_length=100, null=True)
    logo = models.ImageField(null=True, upload_to='companies/logos/')
    created_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return str(self.id)
