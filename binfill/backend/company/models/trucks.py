from __future__ import unicode_literals
from django.db import models
from users.models import User

class Truck(models.Model):

    """Trucks model """
    TRUCK_TYPE_CHOICES = (
        ('HEV', 'Heavy'),
        ('MED', 'Medium'),
        ('SPH', 'Super_heavy')
        )
    display_id = models.CharField(max_length=256, null=True, blank=True)
    truck_type = models.CharField(max_length=50, choices=TRUCK_TYPE_CHOICES)
    number = models.CharField(max_length=100, null=True, blank=True)
    color = models.CharField(max_length=100)
    license_plate = models.CharField(max_length=256)
    vin = models.CharField(max_length=256, unique=True)
    make = models.CharField(max_length=256)
    truck_model = models.CharField(max_length=256)
    kilo_metres = models.CharField(max_length=256)
    fuel_efficiency = models.CharField(max_length=256)
    created_date = models.DateField(auto_now_add=True)
    modified_date = models.DateField(auto_now=True)
    created_by = models.ForeignKey(User, db_index=True, on_delete=models.CASCADE,
                                   related_name='truck_created_by', null=True, blank=True)
    is_deleted = models.BooleanField(default=False)
    deleted_on = models.DateTimeField(auto_now_add=False, null=True)
    company = models.ForeignKey('company.Company', on_delete=models.SET_NULL, null=True)

    def __unicode__(self):
        return self.id
