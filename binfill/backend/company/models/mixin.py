from django.db import models

from .company_details import Company


class CompanyResource(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE,
                                related_name="%(app_label)s_%(class)s_related"
                                )

    class Meta:
        abstract = True
