from .company_contact_person import ContactPerson
from .company_details import Company
# from .company_details import CompanyTransferStation
from .coordinator import Coordinator
from .trucks import Truck
