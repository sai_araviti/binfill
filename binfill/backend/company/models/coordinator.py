from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from company.models.mixin import CompanyResource
from users.models import User


class Coordinator(CompanyResource):
    """
        model for create super cordinator and cordinators
    """

    user = models.OneToOneField(User, on_delete=models.SET_NULL, null=True, blank=True)

    phone = PhoneNumberField(blank=True)

    created_by = models.ForeignKey(User, db_index=True, on_delete=models.CASCADE,
                                   related_name='coordinator_createdby', null=True, blank=True)

    is_super_coordinator = models.BooleanField(default=False,
                                               help_text='Designates whether the user is super cordinator or cordinator.')

    created_date = models.DateTimeField(auto_now=True, null=True)

    deleted_on = models.DateTimeField(auto_now=False, null=True)

    def __unicode__(self):
        return str(self.id)
