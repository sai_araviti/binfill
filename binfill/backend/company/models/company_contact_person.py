# Company contact persons models
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from company.models.company_details import Company
from utils.models.mixins import AddressMixin


class ContactPerson(AddressMixin):
    """Company person model """

    company = models.OneToOneField(Company, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)

    phone_number = PhoneNumberField()
    email = models.EmailField(max_length=100, db_index=True)

    def __unicode__(self):
        return self.id
