from rest_framework import serializers


class TruckChoicesSerializer(serializers.Serializer):
    id = serializers.CharField()
    name = serializers.CharField()
