from rest_framework import serializers
from company.models import trucks,coordinator
from users.serializers.admin import UserSerializer
from company.serializers.company_serializers import CompanySerializer
from company import get_driver


class DisplayId():
    """create dispaly id"""

    def get_dispaly_id(self, truck_type):

        """Get display id for truck"""

        truck_entry = trucks.Truck.objects.filter(truck_type=truck_type).order_by('id').last()
        if truck_entry is None:
            truck_display_id = truck_type + "01"
        else:
            truck_display_id = int(truck_entry.display_id[3:]) + 1
            truck_display_id = truck_type + str("%02d" % truck_display_id)
        return truck_display_id

class TrucksSerializer(serializers.ModelSerializer):

    """Trucks Serializer"""

    def create(self, validated_data):
        truck_type = validated_data.get('truck_type')
        validated_data['display_id'] = DisplayId().get_dispaly_id(truck_type)
        company_list = coordinator.Coordinator.objects.filter(user=self.context['request'].user).order_by('id')[0]
        validated_data['company'] = company_list.company
        validated_data['created_by'] = self.context['request'].user
        new_truck = trucks.Truck.objects.create(**validated_data)
        return new_truck

    class Meta:
        model = trucks.Truck
        fields = ('__all__')

class TrucksListSerializer(serializers.ModelSerializer):

    """Truck List Serializer """
    driver = serializers.SerializerMethodField(read_only=True)
    created_by = UserSerializer()
    company = CompanySerializer()

    def get_driver(self, obj):
        driver = get_driver.GetDriver().drivers_list(obj)
        return driver

    class Meta:
        model = trucks.Truck
        exclude = ()
