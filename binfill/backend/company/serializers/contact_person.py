"""company contact person serializers"""
from rest_framework import serializers

from company.models import ContactPerson
from utils.serializers.cities import CountrySerializer, ProvinceSerializer, CitySerializer
from company.serializers.company_serializers import CompanySerializer


class ContactPersonSerializer(serializers.ModelSerializer):
    """Company Serializer for creating and updating admins """

    class Meta:
        model = ContactPerson
        fields = ('__all__')


class ContactPersonListSerializer(serializers.ModelSerializer):
    """ Company List Serializer"""

    country = CountrySerializer()
    province = ProvinceSerializer()
    city = CitySerializer()
    company =CompanySerializer()

    class Meta:
        model = ContactPerson
        fields = ('__all__')
