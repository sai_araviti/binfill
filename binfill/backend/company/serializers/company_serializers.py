from rest_framework import serializers
from company.models.company_details import Company
from utils.serializers.cities import CitySerializer, CountrySerializer, ProvinceSerializer


class CompanySerializer(serializers.ModelSerializer):
    """Company Serializer for creating and updating"""
    created_by = serializers.PrimaryKeyRelatedField(read_only=True, default=serializers.CurrentUserDefault())
    def validate(self, attrs):
        """
        Company validation
        """
        email = attrs.get('email', "")
        name = attrs.get('name', "")
        binfill_page = attrs.get('binfill_page', "")

        if hasattr(self.instance, 'id'):
            if Company.objects.filter(email=email).exclude(id = self.instance.id).exists():
                raise serializers.ValidationError('email id already exists for this company')
            if Company.objects.filter(name=name).exclude(id = self.instance.id).exists():
                raise serializers.ValidationError('company name already exists for this company')
            if Company.objects.filter(binfill_page=binfill_page).exclude(id = self.instance.id).exists():
                raise serializers.ValidationError('binfill_page already exists for this company')

        return attrs

    def create(self, validated_data):
        created_by = self.context['request'].user
        transfer_stations = validated_data.get('transfer_stations')
        size = validated_data.get('size')
        name = validated_data.get('name')
        binfill_page = validated_data.get('binfill_page')
        email = validated_data.get('email')
        phone = validated_data.get('phone')
        hst_reg = validated_data.get('hst_reg')
        logo = validated_data.get('logo')
        country = validated_data.get('country')
        province = validated_data.get('province')
        city = validated_data.get('city')
        street = validated_data.get('street')
        postal_code = validated_data.get('postal_code')

        new_user = Company.objects.create(name=name,size=size, binfill_page=binfill_page,
                                          email=email, phone=phone, hst_reg=hst_reg, logo=logo,
                                          country=country, province=province, city=city,street=street,
                                          postal_code=postal_code, created_by=created_by)

        new_user.transfer_stations.add(*transfer_stations)
        return new_user

    class Meta:
        model = Company
        fields = ('__all__')



class CompanyListSerializer(serializers.ModelSerializer):
    """ Company List Serializer"""

    country = CountrySerializer()
    province = ProvinceSerializer()
    city = CitySerializer()

    class Meta:
        model = Company
        fields = ('__all__')
        depth =1
