from rest_framework import serializers
from company.models  import Coordinator
from utils.shortcuts import get_host
from users.serializers.admin import UserSerializer
from users.models.user import ResetPasswordToken
from django.template.loader import render_to_string
from utils.email import send_email
from users.models import User

class CoordinatorSerializer(serializers.ModelSerializer):
    '''
    create coordinator
    '''
    user = UserSerializer(required=True)

    def validate(self, attrs):
        '''
        coordinator validation
        '''
        user_object = attrs.get('user')
        email = user_object['email']
        if hasattr(self.instance, 'user'):
            if hasattr(self.instance.user, 'id'):
                if User.objects.filter(email = email).exclude(id = self.instance.user.id).exists():
                    raise serializers.ValidationError('email id already exists')
        elif User.objects.filter(email = email).exists():
            raise serializers.ValidationError("email id already exists")
        return attrs

    def create(self, validated_data):

        message = "We have sent a email to your mail_id {} containing link to create password".format(validated_data["user"]["email"])

        user_data = validated_data.pop("user")
        logged_in_user = self.context['request'].user

        user = UserSerializer.create(UserSerializer(), validated_data=user_data)

        validated_data['created_by_id'] = logged_in_user.id
        coordinator_user = Coordinator.objects.create(user=user, **validated_data)

        password_set_link = "{url}{token}".format(
            url= "http://"+get_host(self.context['request']) + "/company/create-password-request?resetToken=",
            token=ResetPasswordToken.get_or_genreate_token(user)
        )
        ctx = {
            'first_name': user.first_name,
            'last_name': user.last_name,
            'password_set_link': password_set_link,
            'admin_user': logged_in_user.first_name +" "+logged_in_user.last_name
        }

        print(password_set_link)
        html_content = render_to_string('create_coordinator_email.html', ctx)

        send_email("Your Binfill account is one step away.", html_content, [user.email])

        return coordinator_user

    def update(self, instance, validated_data):
        '''
        coordinator updation
        '''
        user_data = validated_data.pop('user')
        instance.user.first_name = user_data.get('first_name', instance.user.first_name)
        instance.user.last_name = user_data.get('last_name', instance.user.last_name)
        instance.user.email = user_data.get('email', instance.user.email)
        instance.user.save()

        instance.phone = validated_data.get('phone', instance.phone)
        instance.company = validated_data.get('company', instance.company)
        instance.save()
        return instance


    class Meta:

        model = Coordinator
        fields = ('__all__')


