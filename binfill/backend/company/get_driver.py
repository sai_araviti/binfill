# get driver list
from users.models import drivers
from  users.serializers import drivers as driver_serializer

class GetDriver():

    def drivers_list(self,obj):
        data = drivers.Driver.objects.filter(truck=obj)
        serializer_class = driver_serializer.DriversSerializer(data,many=True)
        serialized_data = serializer_class.data
        return serialized_data
