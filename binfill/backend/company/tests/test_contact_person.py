from users.models import *
from users.models.user import User

from rest_framework import status
from rest_framework.test import APITestCase

from django.core import management

import json

class ContactPersonTestCase(APITestCase):
    fixtures = ["company.json", "city.json", "users.json", "transfer_station.json", "admin.json"]

    def setUp(self):
        self.user = User.objects.get(email='adminuser@gmail.com')
        self.client.force_authenticate(user=self.user)
        self.content_type = "application/json"

    def open_file(self):
        with open("company/fixtures/create_contact_person.json") as f:
            create_company = json.load(f)
        return create_company

    def test_company_create(self):
        # create company contact person api test case
        content = self.open_file()
        response = self.client.post("/company/contact-person", json.dumps(content), content_type=self.content_type)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_company_list(self):
        # get company contact person list api test case
        management.call_command('loaddata', 'contact_person.json', verbosity=0)
        response = self.client.get('/company/contact-person/1')
        self.assertEqual(response.status_code, 200)

    def test_company_update(self):
        # update company contact person api test case
        management.call_command('loaddata', 'contact_person.json', verbosity=0)
        content = self.open_file()
        content["email"] = "rodneymax@yahoo.co.in"
        response = self.client.put('/company/contact-person/1', json.dumps(content), content_type=self.content_type)
        self.assertEqual(response.status_code, 200)

    def test_company_delete(self):
        #delete company contact person api test case
        management.call_command('loaddata', 'contact_person.json', verbosity=0)
        response = self.client.delete('/company/contact-person/1')
        self.assertEqual(response.status_code, 405)





