from users.models import *
from users.models.user import User

from rest_framework import status
from rest_framework.test import APITestCase
import json

class BinSetUp(APITestCase):
    fixtures = ["users.json", "company.json", "city.json", "coordinator.json", "truck.json"]
    def setUp(self):
        self.user = User.objects.get(email='supercoordinator@outlook.com')
        self.client.force_authenticate(user=self.user)
        self.content_type = "application/json"

class TruckTestCase(BinSetUp):

    def open_file(self):
        with open("company/fixtures/create_truck.json") as f:
            create_coordinator = json.load(f)
        return create_coordinator

    def test_truck_create(self):
        # create truck api test case
        content = self.open_file()
        response = self.client.post("/company/trucks", json.dumps(content), content_type=self.content_type)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_truck_list(self):
        # get truck list api test case
        response = self.client.get('/company/trucks')
        self.assertEqual(response.status_code, 200)

    def test_truck_update(self):
        # truck update api test case
        content = self.open_file()
        content['color'] = "red"
        response = self.client.put('/company/trucks/1', json.dumps(content), content_type=self.content_type)
        self.assertEqual(response.status_code, 200)

    def test_truck_delete(self):
        # truck delete api test case
        response = self.client.delete('/company/trucks/1')
        self.assertEqual(response.status_code, 200)

class TruckCSVTestCase(BinSetUp):

    def test_truck_CSVList(self):
        # get truck csv list api test case
        response = self.client.get('/company/truck-csv')
        self.assertEqual(response.status_code, 200)

class TruckTypeTestCase(BinSetUp):

    def test_truck_type(self):
        # get truck type list api test case
        response = self.client.get('/company/truck-type')
        self.assertEqual(response.status_code, 200)




