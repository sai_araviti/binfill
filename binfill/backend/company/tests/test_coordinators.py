from users.models import *
from users.models.user import User

from rest_framework import status
from rest_framework.test import APITestCase
from django.conf import settings
from django.template.loader import render_to_string
from django.core import mail
import json

class CoordinatorTest(APITestCase):
    fixtures = ["users.json", "company.json", "city.json", "coordinator.json"]

    def setUp(self):
        self.user = User.objects.get(email='supercoordinator@outlook.com')
        self.client.force_authenticate(user=self.user)
        self.content_type = "application/json"

    def open_file(self):
        with open("company/fixtures/create_coordinator.json") as f:
            create_coordinator = json.load(f)
        return create_coordinator

    def test_coordinator_create(self):
        # create coordinator api test case
        content = self.open_file()
        response = self.client.post("/company/coordinator", json.dumps(content),  content_type=self.content_type)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_coordinator_list(self):
        # get coordinator list api test case
        response = self.client.get('/company/coordinator/1')
        self.assertEqual(response.status_code, 200)

    def test_coordinator_update(self):
        # coordinator update api test case
        content = self.open_file()
        content['user']['email'] = "nimjeyson@outlook.com"
        response = self.client.put('/company/coordinator/1', json.dumps(content), content_type=self.content_type)
        self.assertEqual(response.status_code, 200)

    def test_coordinator_delete(self):
        #coordinator delete api test case
        response = self.client.delete('/company/coordinator/2')
        self.assertEqual(response.status_code, 200)

    def test_mail(self):
        # coordinator mail checking test case
        ctx = {
            'first_name': "john",
            'last_name': "catalina",
            'password_set_link': "http://192.168.1.96:8000/password_reset_link",
            'admin_user': self.user.first_name + " " + self.user.last_name
        }
        html_content = render_to_string('create_coordinator_email.html', ctx)
        msg = mail.EmailMultiAlternatives(subject="user mail test case", from_email=settings.EMAIL_HOST_USER, to=[self.user.email])
        msg.attach_alternative(html_content, "text/html")
        msg.send()

        self.assertEqual(len(mail.outbox), 1)

        self.assertEqual(mail.outbox[0].subject, 'user mail test case')




