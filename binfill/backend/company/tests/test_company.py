from users.models import *
from company.models.company_details import Company
from users.models.user import User

from rest_framework import status
from rest_framework.test import APITestCase
from django.test.client import encode_multipart
from django.core.files.uploadedfile import SimpleUploadedFile
import json

class CompanyTestCase(APITestCase):
    fixtures = ["company.json", "city.json", "users.json", "transfer_station.json", "admin.json"]

    def setUp(self):
        self.user = User.objects.get(email='adminuser@gmail.com')
        self.client.force_authenticate(user=self.user)

    def create_company(self):
        self.create_country_province_city()
        return Company.objects.create(**self.company_details)

    def create_test_company(self):
        self.create_company()
        cc = self.create_company()
        self.assertTrue(isinstance(cc, Company))

    def create_form_data(self, data):
        content = encode_multipart('BoUnDaRyStRiNg', data)
        return content

    def open_file(self):
        logo = SimpleUploadedFile(name = "download.jpeg", content=open("c:/users/cy0106/downloads/download.jpeg", 'rb').read())
        with open("company/fixtures/create_company.json") as f:
            create_company = json.load(f)
            create_company["logo"] = logo
        return create_company

    def test_company_create(self):
        # create company api test case
        create_company = self.open_file()
        content = self.create_form_data(create_company)

        content_type = 'multipart/form-data; boundary=BoUnDaRyStRiNg'
        response = self.client.post("/company/create_update_company", content,  content_type=content_type)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_company_list(self):
        # get company list api test case
        response = self.client.get('/company/create_update_company/1')
        self.assertEqual(response.status_code, 200)

    def test_company_update(self):
        # company update api test case
        company_details = self.open_file()
        company_details["street"] = "technopark-phase 3--"
        content = self.create_form_data(company_details)
        content_type = 'multipart/form-data; boundary=BoUnDaRyStRiNg'
        response = self.client.put('/company/create_update_company/1', content, content_type=content_type)
        self.assertEqual(response.status_code, 200)

    def test_company_delete(self):
        #company delete api test case
        response = self.client.delete('/company/create_update_company/1')
        self.assertEqual(response.status_code, 200)





