# Create your views here.
import csv
import datetime
import django_filters
from django.http import HttpResponse
from rest_framework import viewsets, filters
from rest_framework.response import Response
from rest_framework import generics
from django.utils import timezone
from company.models import trucks
from company.serializers import trucks as  trucks_serializer
from company.filters import truckfilter
from utils.shortcuts import choice_to_list_of_dict
from company.serializers import helpers

class TruckChoicesViewSet(generics.ListAPIView):
    """Listing Payment Choices Viewset"""

    serializer_class = helpers.TruckChoicesSerializer
    queryset = choice_to_list_of_dict(trucks.Truck.TRUCK_TYPE_CHOICES)


class TruckViewSet(viewsets.ModelViewSet):

    """Trucks viewset"""

    queryset = trucks.Truck.objects.filter(is_deleted=False)
    serializer_class = trucks_serializer.TrucksSerializer
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend, filters.OrderingFilter,
                       filters.SearchFilter,)
    filterset_class = truckfilter.TrucksFilterSet
    search_fields = ['vin', 'color', 'truck_display_id', 'license_plate']
    ordering_fields = '__all__'

    def get_queryset(self):

        """overriding truck queryset"""
        company_id = self.queryset.filter(created_by=self.request.user.id).values('company')
        queryset = trucks.Truck.objects.filter(company__in=company_id,is_deleted=False)
        return queryset

    def get_serializer_class(self):

        if self.action == 'retrieve' or self.action == 'list':
            serializer = trucks_serializer.TrucksListSerializer
        else:
            serializer = trucks_serializer.TrucksSerializer
        return serializer

    def destroy(self, request, *args, **kwargs):

        """ deleting trucks """

        instance = self.get_object()
        instance.deleted_on = timezone.now()
        instance.is_deleted = True
        instance.company = None
        instance.save()
        return Response({'message':'success'})


class TruckCsvDownloadViewSet(viewsets.ViewSet):

    def list(self, request):
        query_params = self.request.query_params
        truck_type = query_params.get('truck_type', None)
        color = query_params.get('color', None)
        license_plate = query_params.get('license_plate', None)
        vin = query_params.get('vin', None)
        make = query_params.get('make', None)
        truck_model = query_params.get('truck_model', None)

        company_id = trucks.Truck.objects.filter(created_by=self.request.user.id).values('company')
        truck_details = trucks.Truck.objects.filter(company__in=company_id)

        if truck_type:
            truck_details = truck_details.filter(truck_type=truck_type)
        if color:
            truck_details = truck_details.filter(color=color)
        if license_plate:
            truck_details = truck_details.filter(license_plate=license_plate)
        if vin:
            truck_details = truck_details.filter(vin=vin)
        if make:
            truck_details = truck_details.filter(make=make)
        if truck_model:
            truck_details = truck_details.filter(truck_model=truck_model)

        file_name = "truck_%s.csv" % (datetime.datetime.now())
        response = HttpResponse(content_type='application/csv')
        response['Content-Disposition'] = 'attachment; filename=%s'%(file_name)

        writer = csv.writer(response)
        writer.writerow(['Truck Display id', 'Truck Type', 'Truck Number', 'color', 'license_plate',
                         'vin', 'make', 'truck_model', 'kilo_metres', 'fuel_efficiency'])
        field_list = [[i+1, f.display_id, f.truck_type, f.number, f.color,
                       f.license_plate, f.vin, f.make, f.truck_model, f.kilo_metres,
                       f.fuel_efficiency]for i, f in enumerate(truck_details) if f]
        writer.writerows(field_list)
        return response
