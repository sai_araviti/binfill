from company.models import Coordinator
from company.serializers.coordinator_serializers import CoordinatorSerializer
from users.serializers.forgot_password import ChangePasswordSerializer
from utils.shortcuts import success, failure
from users.models import user
from rest_framework import viewsets, filters
from django.db.models import Q
from rest_framework.response import Response
from django.utils import timezone

class CoordinatorView(viewsets.ModelViewSet):
    '''
        create super cordinator
    '''
    queryset = Coordinator.objects.all()
    serializer_class = CoordinatorSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('user__email', 'phone', 'user__first_name', 'user__last_name')
    ordering_fields = '__all__'

    def list(self, request):
        '''
        Get list of coordinators
        '''
        coordinator_objects = self.queryset.filter(company_id = self.request.user.coordinator.company, deleted_on__isnull = True)
        serializer= CoordinatorSerializer(coordinator_objects, many=True)
        return Response(serializer.data)

    def destroy(self, request, pk = None):
        '''
        delete coordinator
        '''
        if self.queryset.filter(Q(is_super_coordinator = True) | Q(user = request.user.id), id = pk).exists():
            return success(message = "you can't delete supercordinator and your own record")
        else:
            self.queryset.filter(id = pk).update(deleted_on = timezone.now())
            self.queryset.get(id = pk).user.delete()
        return success(message = "coordinator deleted successfully")

class CreateCoordinatorPasswordView(viewsets.ModelViewSet):
    '''
    create the password of a user new password
    '''
    queryset = user.ResetPasswordToken.objects.all()
    serializer_class = ChangePasswordSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        #create new password
        if serializer.is_valid():
            serializer.save()
            return success(message="Your password has been created successfully")
        else:
            return failure(serializer.errors)



