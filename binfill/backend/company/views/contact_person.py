# Create your views here.
from rest_framework import viewsets
from company import models as contact_person
from company.serializers import contact_person as contact_person_serializer

class ContactPersonViewSet(viewsets.ModelViewSet):

    """Company contact person viewset"""

    queryset = contact_person.ContactPerson.objects.all()
    serializer_class = contact_person_serializer.ContactPersonSerializer
    http_method_names = ['get', 'post', 'put']

    def get_serializer_class(self):

        if self.action == 'retrieve' or self.action == 'list':
            serializer = contact_person_serializer.ContactPersonListSerializer

        else:
            serializer = contact_person_serializer.ContactPersonSerializer

        return serializer

