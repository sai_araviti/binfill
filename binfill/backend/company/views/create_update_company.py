# Create your views here.
from rest_framework import viewsets
from rest_framework.parsers import MultiPartParser, FormParser
from company.serializers import company_serializers
from company.models import Company
from utils.shortcuts import success
from company.models import Coordinator, Truck
from payments.models import CardDetails
from users.models import User, Driver
from bins.models import Bin
from django.utils import timezone

class CompanyViewSet(viewsets.ModelViewSet):
    '''
        Company view set
    '''
    queryset = Company.objects.all()
    serializer_class = company_serializers.CompanySerializer
    parser_classes = (MultiPartParser,)

    def get_serializer_class(self):

        if self.action == 'retrieve' or self.action == 'list':
            serializer = company_serializers.CompanyListSerializer
        else:
            serializer = company_serializers.CompanySerializer
        return serializer

    def destroy(self, request, pk = None):
        coordinators = []
        message = "you don't have permission to delete the company"
        company = self.get_object()
        company.deleted_on = timezone.now()
        company.is_deleted = True

        if hasattr(request.user, "adminuser"):

            company_users = Coordinator.objects.filter(company = pk, deleted_on__isnull = True).values("user_id")
            coordinators = [user_id['user_id'] for user_id in company_users]
            company_users.update(deleted_on = timezone.now())
            Bin.objects.filter(company = pk).update(deleted_on = timezone.now(), is_deleted= True)
            Truck.objects.filter(company = pk).update(deleted_on = timezone.now(), is_deleted= True)
            Driver.objects.filter(company = pk).update(deleted_on = timezone.now(), is_deleted= True)
            CardDetails.objects.filter(company = pk).delete()

            User.objects.filter(pk__in = coordinators).delete()
            message = "selected company deleted successfully"
            company.save()

        return success(data=None, message=message, status=None)
