from django.conf.urls import include, url
from rest_framework_nested import routers
from company.views import create_update_company, coordinator ,trucks
from .views import  contact_person

router = routers.SimpleRouter(trailing_slash=False)
router.register('contact-person', contact_person.ContactPersonViewSet, 'city')
router.register('create_update_company', create_update_company.CompanyViewSet, 'create_update_company')
router.register('coordinator', coordinator.CoordinatorView, 'coordinator')
router.register('create-password-request', coordinator.CreateCoordinatorPasswordView, 'coordinator_password_creation')
router.register('trucks', trucks.TruckViewSet, 'trucks')
router.register('truck-csv', trucks.TruckCsvDownloadViewSet, 'truck_csv')

"""
Company module urls - starts with 'company'

"""

urlpatterns = [
    url(r'^', include(router.urls)),
    url('truck-type', trucks.TruckChoicesViewSet.as_view())
]
