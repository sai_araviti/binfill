import django_filters
from company.models import trucks

class TrucksFilterSet(django_filters.rest_framework.FilterSet):

    """Filter class for trucks module"""

    id = django_filters.NumberFilter(field_name="id")
    truck_display_id = django_filters.CharFilter(field_name="truck_display_id")
    truck_type = django_filters.CharFilter(field_name="truck_type")
    color = django_filters.CharFilter(field_name="color")
    license_plate = django_filters.CharFilter(field_name="license_plate")
    vin = django_filters.CharFilter(field_name="vin")
    make = django_filters.CharFilter(field_name="make")
    truck_model = django_filters.CharFilter(field_name="truck_model")
    kilo_metres = django_filters.CharFilter(field_name="kilo_metres")
    fuel_efficiency = django_filters.CharFilter(field_name="fuel_efficiency")
    company__name = django_filters.CharFilter(field_name="company__name")


    class Meta:
        model = trucks.Truck
        fields = ['id', 'truck_display_id', 'truck_type', 'color',
                  'license_plate', 'vin', 'make', 'truck_model', 'kilo_metres',
                  'fuel_efficiency', 'company__name']
