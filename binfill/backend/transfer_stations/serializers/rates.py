from rest_framework import serializers
from transfer_stations.models import rates, materials

class RateSerializer(serializers.ModelSerializer):

    """Serializer for rate management"""

    def create(self, validated_data):

        material_list = validated_data.get('material')
        waste_types = validated_data.get('waste_type')
        delivery_material = validated_data.get('delivery_material')
        is_dump_station = validated_data.get('is_dump_station')
        transfer_station = validated_data.get('transfer_station')
        rate = rates.CompanyRatesManagement.objects.create(delivery_material=delivery_material,is_dump_station=is_dump_station,
                                             transfer_station=transfer_station)
        rate.material.add(*material_list)
        rate.waste_type.add(*waste_types)
        return rate

    class Meta:
        model = rates.CompanyRatesManagement
        fields = ('__all__')

class RateListSerializer(serializers.ModelSerializer):

    """Serializer for listing rate management"""

    class Meta:
        model = rates.CompanyRatesManagement
        fields = ('__all__')
        depth=1


class MaterialRateSerializer(serializers.ModelSerializer):

    """Serializer for materialrate model """
    class Meta:
        model = rates.MaterialRate
        fields = ('__all__')

class WasteDisposalSerializer(serializers.ModelSerializer):

    """Serializer for waste disposal model """
    class Meta:
        model = rates.WasteDisposalRate
        fields = ('__all__')
