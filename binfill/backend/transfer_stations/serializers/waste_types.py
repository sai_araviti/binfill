from rest_framework import serializers
from transfer_stations.models import waste_types

class WasteTypeSerializer(serializers.ModelSerializer):

    """Serializer for waste type model """

    class Meta:
        model = waste_types.WasteType
        fields = ('__all__')


