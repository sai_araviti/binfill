from rest_framework import serializers
from transfer_stations.models import transfer_stations

class TransferStationSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        transfer_station_entry = transfer_stations.TransferStation.objects.all().order_by('id').last()
        if transfer_station_entry is None:
            validated_data['display_id'] = "TS001"

        else:
            display_id = int(transfer_station_entry.display_id[2:]) + 1
            validated_data['display_id'] = "TS" + str("%03d" % display_id)
        validated_data['created_by'] = self.context['request'].user
        new_user = transfer_stations.TransferStation.objects.create(**validated_data)
        return new_user

    def update(self, instance, validated_data):

        instance.name = validated_data.get('name', instance.name)
        instance.own_yard = validated_data.get('own_yard', instance.own_yard)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.email = validated_data.get('email', instance.email)
        instance.postal_code = validated_data.get('postal_code', instance.postal_code)
        instance.street = validated_data.get('street', instance.street)
        instance.country = validated_data.get('country', instance.country)
        instance.province = validated_data.get('province', instance.province)
        instance.city = validated_data.get('city', instance.city)
        instance.edited_by_id = self.context['request'].user.id
        instance.save()
        return instance

    class Meta:
        model = transfer_stations.TransferStation
        fields = ('__all__')
