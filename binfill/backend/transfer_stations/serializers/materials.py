from rest_framework import serializers
from transfer_stations.models import materials

class MaterialSerializer(serializers.ModelSerializer):

    """Serializer for material model """

    class Meta:
        model = materials.Material
        fields = ('__all__')




