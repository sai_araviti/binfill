from django.apps import AppConfig


class TransferstationsConfig(AppConfig):
    name = 'transfer_stations'
