from django.conf.urls import include, url
from rest_framework_nested import routers
from transfer_stations.views import transfer_stations, materials,waste_types, rates
from company.views import create_update_company

router = routers.SimpleRouter(trailing_slash=False)
router.register('transfer-stations', transfer_stations.TransferStationViewSet, 'transfer-stations')
router.register('material', materials.MaterialsViewSet, 'material')
router.register('waste-types', waste_types.WasteTypeViewSet, 'waste-types')
router.register('rate-management', rates.CompanyRateManagementViewSet, 'rate-management')

router.register('company', create_update_company.CompanyViewSet, base_name='company')
company_router = routers.NestedSimpleRouter(router, r'company', lookup='companies')
company_router.register(r'transfer-stations', transfer_stations.TransferStationViewSet, base_name='transfer-stations')

material_router = routers.NestedSimpleRouter(company_router, r'transfer-stations', lookup='transfer_stations')
material_router.register(r'materials', rates.MaterialsRateViewSet, base_name='materials')
material_router.register(r'waste-disposal-rate', rates.WasteDisposalRateViewSet, base_name='waste-disposal-rate')

"""
Transfer Station module urls - starts with 'transfer-stations'

"""

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(company_router.urls)),
    url(r'^', include(material_router.urls)),
    url(r'transfer-stations-csv', transfer_stations.TransferStationCsvViewSet.as_view()),

]
