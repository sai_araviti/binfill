from rest_framework.test import APITestCase
from django.test import Client
from users.models import User

class MateialtestCase(APITestCase):

    """Test the material module"""

    fixtures = ['users.json','material.json']

    def setUp(self):
        self.user = User.objects.get(pk=1)
        self.client = Client()
        self.client.login(email='adminuser@gmail.com', password='binfill@1234')

    def test_material(self):

        """
        Get materials ..API should return 200 response

        """
        response = self.client.get('/transfer-stations/material')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")

