from rest_framework.test import APITestCase
from django.test import Client
from users.models import User

class TestLicensetype(APITestCase):

    """Test the license module"""

    fixtures = ['users.json','city.json']

    data = {
            "name": "site1",
            "own_yard": "False",
            "phone": "+917025120518",
            "email": "testsite@gmail.com",
            "street": "dfgdfgfd",
            "country": 1,
            "province": 1,
            "city": 1,
            "postal_code": "45656545",
            }


    def setUp(self):
        self.user = User.objects.get(pk=1)
        self.client = Client()
        self.client.login(email='adminuser@gmail.com', password='binfill@1234')

    def test_licensetype(self):

        """
        Get,post license type ..API should return 201 or 200 response

        """
        response = self.client.post('/transfer-stations/transfer-stations', self.data, content_type='application/json')
        self.assertEqual(response.status_code, 201, "API should return 200 OK status")
        response = self.client.get('/transfer-stations/transfer-stations', content_type='application/json')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")

