from rest_framework.test import APITestCase
from django.test import Client
from users.models import User

class CompanyRateManagementTestCase(APITestCase):

    """Test the rate management module"""

    fixtures = ['users.json','material.json','wastetype.json','transferstation.json','city.json','ratesmanagement.json']

    data ={
        "delivery_material": False,
        "is_dump_station": True,
        "transfer_station": 1,
        "material": [
                1,
                2
            ],
        "waste_type": [
            1,
            2
        ]
    }

    def setUp(self):
        self.user = User.objects.get(pk=1)
        self.client = Client()
        self.client.login(email='adminuser@gmail.com', password='binfill@1234')

    def test_material(self):
        """
        Get rate management ..API should return 200 response
        """
        response = self.client.post('/transfer-stations/rate-management', self.data, content_type='application/json')
        self.assertEqual(response.status_code, 201, "API should return 200 OK status")
        response = self.client.get('/transfer-stations/rate-management')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")
        self.data['material'] =[1]
        response = self.client.put('/transfer-stations/rate-management/2', self.data, content_type='application/json')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")
        response = self.client.get('/transfer-stations/rate-management/2')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")

class MaterialsRateTestCase(APITestCase):

    """Test the rate management module"""

    fixtures = ['users.json','material.json','wastetype.json','transferstation.json','city.json','ratesmanagement.json',
                'company.json','materialrates.json']

    data ={
        "company":2,
        "transfer_station":1,
        "material":[1,2],
        "rate":1.23
    }

    def setUp(self):
        self.user = User.objects.get(pk=1)
        self.client = Client()
        self.client.login(email='adminuser@gmail.com', password='binfill@1234')

    def test_materialrate(self):

        """
        Get material rate management ..API should return 200 response
        """
        response = self.client.post('/transfer-stations/company/2/transfer-stations/1/materials/', self.data, content_type='application/json')
        self.assertEqual(response.status_code, 201, "API should return 200 OK status")
        response = self.client.get('/transfer-stations/company/2/transfer-stations/1/materials/')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")
        self.data['material'] =[1]
        response = self.client.put('/transfer-stations/company/2/transfer-stations/1/materials/2/', self.data, content_type='application/json')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")
        response = self.client.get('/transfer-stations/company/2/transfer-stations/1/materials/2/')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")

class WasteDisposalRateTestCase(APITestCase):

    """Test the rate management module"""

    fixtures = ['users.json','wastetype.json','material.json','transferstation.json','city.json','ratesmanagement.json',
                'company.json','materialrates.json']

    data ={
        "company":2,
        "transfer_station":1,
        "waste":[1,2],
        "rate":1.23
    }

    def setUp(self):
        self.user = User.objects.get(pk=1)
        self.client = Client()
        self.client.login(email='adminuser@gmail.com', password='binfill@1234')

    def test_wastetyperate(self):

        """
        Get material rate management ..API should return 200 response
        """
        response = self.client.post('/transfer-stations/company/2/transfer-stations/1/waste-disposal-rate/', self.data, content_type='application/json')
        self.assertEqual(response.status_code, 201, "API should return 200 OK status")
        response = self.client.get('/transfer-stations/company/2/transfer-stations/1/waste-disposal-rate/')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")
        self.data['waste'] =[1]
        response = self.client.put('/transfer-stations/company/2/transfer-stations/1/waste-disposal-rate/1/', self.data, content_type='application/json')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")
        response = self.client.get('/transfer-stations/company/2/transfer-stations/1/waste-disposal-rate/1/')
        self.assertEqual(response.status_code, 200, "API should return 200 OK status")