import django_filters
from transfer_stations.models import transfer_stations

class TransferStationFilterSet(django_filters.rest_framework.FilterSet):

    """Filter class for trucks module"""
    name = django_filters.CharFilter(field_name="name")
    display_id = django_filters.NumberFilter(field_name="display_id")
    own_yard = django_filters.BooleanFilter(field_name="id")
    phone = django_filters.NumberFilter(field_name="phone")
    email = django_filters.CharFilter(field_name="email")

    class Meta:
        model = transfer_stations.TransferStation
        fields = ['name', 'display_id', 'own_yard', 'phone', 'email']
