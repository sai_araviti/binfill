from rest_framework import viewsets
from transfer_stations.models import waste_types
from transfer_stations.serializers import waste_types as waste_type_serializer

class WasteTypeViewSet(viewsets.ModelViewSet):

    """ Waste Type Viewset """

    queryset = waste_types.WasteType.objects.all()
    serializer_class = waste_type_serializer.WasteTypeSerializer
    http_method_names = ['get']


