from rest_framework import viewsets
from transfer_stations.models import materials
from transfer_stations.serializers import materials as material_serializer

class MaterialsViewSet(viewsets.ModelViewSet):

    """ Material Viewset """

    queryset = materials.Material.objects.all()
    serializer_class = material_serializer.MaterialSerializer
    http_method_names = ['get']


