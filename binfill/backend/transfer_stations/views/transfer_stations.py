# Create your views here.
from rest_framework import viewsets, filters
import django_filters
import csv
import datetime
from django.http import HttpResponse
from django.utils import timezone
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from transfer_stations.models import transfer_stations, rates
from transfer_stations.serializers import transfer_stations as transfer_station_serializer
from transfer_stations.filters import transfer_station as transfer_stations_filter

class TransferStationViewSet(viewsets.ModelViewSet):

    """Transfer Station viewset"""
    queryset = transfer_stations.TransferStation.objects.filter(is_deleted=False)
    serializer_class = transfer_station_serializer.TransferStationSerializer
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend, filters.OrderingFilter,
                       filters.SearchFilter,)
    filterset_class = transfer_stations_filter.TransferStationFilterSet
    search_fields = ['name', 'display_id', 'own_yard', 'phone', 'email',
                     'city', 'country', 'street', 'province', 'postal_code']
    ordering_fields = '__all__'

    def destroy(self, request, *args, **kwargs):

        """ deleting transfer station """

        instance = self.get_object()
        instance.deleted_on = timezone.now()
        instance.is_deleted = True
        instance.save()
        return Response({'message':'success'})



class TransferStationCsvViewSet(ListAPIView):
    '''
    Get Transfer Station list into csv
    '''
    def list(self, request):
        queryset = transfer_stations.TransferStation.objects.filter(is_deleted=False)
        file_name = "transfer_stations%s.csv" % (datetime.datetime.now())
        response = HttpResponse(content_type='application/csv')
        response['Content-Disposition'] = 'attachment; filename=%s'%(file_name)

        writer = csv.writer(response)
        writer.writerow(['Transfer Station ID', 'Name', 'Address', 'City','Is own yard', 'Does deliver material'])
        field_list = [[i+1, f.site_id, f.name, str(f.country.name)+', '+str(f.province.name) \
        +' '+str(f.postal_code)+', '+str(f.city)+','+str(f.street), str(f.city),f.own_yard,
        rates.RateModel.objects.filter(transfer_station=f.id).values('is_dump_station')[0]['is_dump_station']]
                      for i, f in enumerate(queryset) if f]
        writer.writerows(field_list)
        return response