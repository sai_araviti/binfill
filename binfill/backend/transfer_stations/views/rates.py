from rest_framework import viewsets
from transfer_stations.models import rates
from transfer_stations.serializers import rates as rates_serializer

class CompanyRateManagementViewSet(viewsets.ModelViewSet):

    """ Rate Management Viewset """

    queryset = rates.CompanyRatesManagement.objects.all()
    serializer_class = rates_serializer.RateSerializer

    def get_serializer_class(self):

        if self.action == 'retrieve' or self.action == 'list':
            serializer = rates_serializer.RateListSerializer
        else:
            serializer = rates_serializer.RateSerializer
        return serializer

class MaterialsRateViewSet(viewsets.ModelViewSet):

    """MaterialsRate Viewset"""

    queryset = rates.MaterialRate.objects.all()
    serializer_class = rates_serializer.MaterialRateSerializer

    def get_queryset(self):

        """overriding queryset"""
        queryset = self.queryset.filter(company=self.kwargs['companies_pk'],
                                        transfer_station=self.kwargs['transfer_stations_pk'])
        return queryset


class WasteDisposalRateViewSet(viewsets.ModelViewSet):

    """Waste Disposal Rate Viewset"""

    queryset = rates.WasteDisposalRate.objects.all()
    serializer_class = rates_serializer.WasteDisposalSerializer

    def get_queryset(self):

        """overriding queryset"""
        queryset = self.queryset.filter(company=self.kwargs['companies_pk'],
                                        transfer_station=self.kwargs['transfer_stations_pk'])
        return queryset
