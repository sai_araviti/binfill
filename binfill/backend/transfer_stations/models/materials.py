from __future__ import unicode_literals
from django.db import models

class Material(models.Model):

    """Material model """
    name = models.CharField(max_length=256, unique=True)

    def __unicode__(self):
        return self.id


