from __future__ import unicode_literals
from django.db import models

class WasteType(models.Model):

    """Waste Type model """
    name = models.CharField(max_length=256, unique=True)

    def __unicode__(self):
        return self.id


