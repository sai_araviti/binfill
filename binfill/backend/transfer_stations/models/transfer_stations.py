from __future__ import unicode_literals
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from utils.models.mixins import AddressMixin
from users.models import User

class TransferStation(AddressMixin):

    """Transfer Station model """

    name = models.CharField(max_length=256, unique=True)
    display_id = models.CharField(max_length=256, null=True)
    own_yard = models.BooleanField(default=False)
    phone = PhoneNumberField()
    email = models.EmailField(max_length=256, unique=True)
    created_date = models.DateField(auto_now_add=True, null=True)
    modified_date = models.DateField(auto_now_add=False, null=True)
    created_by = models.ForeignKey(User, db_index=True, on_delete=models.PROTECT,
                                   related_name='transfer_station_created_by',
                                   null=True, blank=True)
    is_deleted = models.BooleanField(default=False)
    deleted_on = models.DateTimeField(auto_now_add=False, null=True)

    def __unicode__(self):
        return self.id
