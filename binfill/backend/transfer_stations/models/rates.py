from __future__ import unicode_literals
from django.db import models
from rest_framework.exceptions import ValidationError
from transfer_stations.models import materials
from transfer_stations.models import waste_types
from transfer_stations.models import transfer_stations

class CompanyRatesManagement(models.Model):

    """Rate model """
    delivery_material = models.BooleanField(default=False)
    is_dump_station = models.BooleanField(default=False)
    material = models.ManyToManyField(materials.Material)
    waste_type = models.ManyToManyField(waste_types.WasteType)
    transfer_station = models.ForeignKey(transfer_stations.TransferStation,
                                         on_delete=models.PROTECT)

    def __unicode__(self):
        return self.id

    def save(self, *args, **kwargs):
        if self.delivery_material is False and self.is_dump_station is False:
            raise ValidationError({'data': 'Please select one of the select boxes'})
        else:
            super(CompanyRatesManagement, self).save(*args, **kwargs)


class MaterialRate(models.Model):

    """ Material Rate Model """
    company = models.OneToOneField('company.Company', on_delete=models.PROTECT)
    transfer_station = models.ForeignKey(transfer_stations.TransferStation,
                                         on_delete=models.PROTECT)
    material = models.ManyToManyField(materials.Material)
    is_flat_rate = models.BooleanField(default=False)
    rate = models.DecimalField(decimal_places=2, max_digits=20, null=True)

    def __unicode__(self):
        return self.id


class WasteDisposalRate(models.Model):

    """ Waste Disposal Rate Model """
    company = models.OneToOneField('company.Company', on_delete=models.PROTECT)
    transfer_station = models.ForeignKey(transfer_stations.TransferStation,
                                         on_delete=models.PROTECT)
    waste = models.ManyToManyField(waste_types.WasteType)
    is_flat_rate = models.BooleanField(default=False)
    rate = models.CharField(max_length=256, null=True)

    def __unicode__(self):
        return self.id
