# Company contact persons models
from django.db import models
from company.models.company_details import Company

class CardDetails(models.Model):

    """Company card details model """

    card_id= models.CharField(max_length=256)
    cardholder_name = models.CharField(max_length=256)
    card_number = models.CharField(max_length=256)
    card_type = models.CharField(max_length=256)
    is_primary = models.BooleanField()
    exp_month = models.IntegerField()
    exp_year = models.IntegerField()
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    datetime_created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.id
