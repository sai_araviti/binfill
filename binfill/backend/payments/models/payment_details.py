# Company contact persons models
from django.db import models
from utils.models.mixins import AddressMixin
from company.models.company_details import Company

class PaymentDetails(AddressMixin):

    PAYMENT_CHOICES = (
        ('cheque', 'Cheque'),
        ('credit_card', 'Credit Card')
    )

    SUBSCRIPTION_FEE_CHOICES = (
        ('bi_weekly', 'Bi-weekly'),
        ('weekly', 'Weekly'),
        ('semi_monthly', 'Semi-monthly'),
        ('monthly', 'Monthly'),
    )

    """Company payments model """

    subscription_fee = models.DecimalField(decimal_places=2, max_digits=9)
    subcription_fee_frequency = models.CharField(max_length=50, choices=
                                                  SUBSCRIPTION_FEE_CHOICES, default='monthly')
    payment_choices = models.CharField(max_length=50, choices=PAYMENT_CHOICES)
    address = models.TextField()
    emails = models.TextField()
    company = models.OneToOneField(Company, on_delete=models.CASCADE)

    def __unicode__(self):
        return self.id


