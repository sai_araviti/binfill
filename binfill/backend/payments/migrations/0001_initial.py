# Generated by Django 2.1 on 2018-09-14 11:29

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.CITIES_COUNTRY_MODEL),
        ('cities', '0011_auto_20180108_0706'),
        migrations.swappable_dependency(settings.CITIES_CITY_MODEL),
        ('company', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CardDetails',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('card_id', models.CharField(max_length=256)),
                ('cardholder_name', models.CharField(max_length=256)),
                ('card_number', models.CharField(max_length=256)),
                ('card_type', models.CharField(max_length=256)),
                ('exp_month', models.IntegerField()),
                ('exp_year', models.IntegerField()),
                ('datetime_created', models.DateTimeField(auto_now_add=True)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='company.Company')),
            ],
        ),
        migrations.CreateModel(
            name='PaymentDetails',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('street', models.CharField(max_length=100)),
                ('postal_code', models.CharField(max_length=10)),
                ('subscription_fee', models.DecimalField(decimal_places=2, max_digits=9)),
                ('subcription_fee_frequency', models.CharField(choices=[('bi_weekly', 'Bi-weekly'), ('weekly', 'Weekly'), ('semi_monthly', 'Semi-monthly'), ('monthly', 'Monthly')], default='monthly', max_length=50)),
                ('payment_choices', models.CharField(choices=[('cheque', 'Cheque'), ('credit_card', 'Credit Card')], max_length=50)),
                ('address', models.TextField()),
                ('emails', models.TextField()),
                ('city', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='payments_paymentdetails_related', to=settings.CITIES_CITY_MODEL)),
                ('company', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='company.Company')),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='payments_paymentdetails_related', to=settings.CITIES_COUNTRY_MODEL)),
                ('province', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='payments_paymentdetails_related', to='cities.Region')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
