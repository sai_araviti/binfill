"""Payment details serializer"""
import datetime

import stripe
from rest_framework import serializers

from company.serializers.company_serializers import CompanySerializer
from config.base import STRIPE_SECRET_KEY
from payments.models import card_details as card_details_models
from payments.models import payment_details
from utils.serializers.cities import CitySerializer, ProvinceSerializer, CountrySerializer


class CreditCardSerializer(serializers.ModelSerializer):
    """credit card serializer"""

    def validate(self, data):

        if data['exp_month'] < 0 or data['exp_month'] > 12:
            raise serializers.ValidationError("Provide valid expiry month")

        if int(data['exp_year']) < int(datetime.datetime.now().year):
            raise serializers.ValidationError("Provide valid expiry year")

        return data

    class Meta:
        model = card_details_models.CardDetails
        fields = ('__all__')


class PaymentDetailsSerializer(serializers.ModelSerializer):
    """Payment Serializer for creating and updating company payments details """
    stripe.api_key = STRIPE_SECRET_KEY

    def validate(self, data):
        """
        Subscription fee is zero or positive numbers
        """
        if data['subscription_fee'] < 0:
            raise serializers.ValidationError("Subscription fee accepts zero and positive numbers")
        return data

    def create(self, validated_data):
        request = self.context.get('request')
        card_details = request.data.get('credit_card_details')
        emails = request.data.get('emails')
        if not len(card_details):
            raise serializers.ValidationError({'credit_card_details':
                                                   'Please enter credit card details'})
        if any(card['is_primary'] is True for card in card_details):
            for cards in card_details:
                customer = stripe.Customer.create(
                    source='tok_mastercard',
                    email=emails,
                    metadata=cards
                )
                cards['card_number'] = (cards['card_number'][-4:].rjust(len(cards['card_number']), "*"))
                cards['card_id'] = customer['id']
            serializer = CreditCardSerializer(data=card_details, many=True)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            payment = payment_details.PaymentDetails.objects.create(**validated_data)
        else:
            raise serializers.ValidationError({'is_primary': 'Cannot delete primary card'})
        return payment

    def update(self, instance, validated_data):
        request = self.context.get('request')
        card_details = request.data.get('credit_card_details')
        emails = request.data.get('emails')
        if not len(card_details):
            raise serializers.ValidationError({'credit_card_details':
                                                   'Please enter credit card details'})
        if any(card['is_primary'] is True for card in card_details):
            old_cards = card_details_models.CardDetails.objects.filter(company=instance.company.id)

            for card_info in old_cards:
                customer = stripe.Customer.retrieve(card_info.card_id)
                customer.sources.retrieve(customer.sources.data[0]['id']).delete()
            card_details_models.CardDetails.objects.filter(company=instance.company.id).delete()

            for cards in card_details:
                customer = stripe.Customer.create(
                    source='tok_mastercard',
                    email=emails,
                    metadata=cards)
                cards['card_number'] = (cards['card_number'][-4:].rjust(len(cards['card_number']), "*"))
                cards['card_id'] = customer['id']
            serializer = CreditCardSerializer(data=card_details, many=True)
            serializer.is_valid(raise_exception=True)
            serializer.save()

        else:
            raise serializers.ValidationError({'is_primary': 'Cannot delete primary card'})

        instance.subscription_fee = validated_data.get('subscription_fee',
                                                       instance.subscription_fee)
        instance.subcription_fee_frequency = validated_data.get('subcription_fee_frequency',
                                                                instance.subcription_fee_frequency)
        instance.payment_choices = validated_data.get('payment_choices',
                                                      instance.payment_choices)
        instance.address = validated_data.get('address', instance.address)
        instance.emails = validated_data.get('emails', instance.emails)
        instance.country = validated_data.get('country', instance.country)
        instance.province = validated_data.get('province', instance.province)
        instance.city = validated_data.get('city', instance.city)
        instance.street = validated_data.get('street', instance.street)
        instance.postal_code = validated_data.get('postal_code', instance.postal_code)
        instance.save()
        return instance

    class Meta:
        model = payment_details.PaymentDetails
        fields = ('__all__')


class PaymentDetailsListSerializer(serializers.ModelSerializer):
    """Payment Serializer for listing company payments details """
    company = CompanySerializer()
    country = CountrySerializer()
    province = ProvinceSerializer()
    city = CitySerializer()

    class Meta:
        model = payment_details.PaymentDetails
        fields = ('__all__')
