from rest_framework import serializers


class PaymentTypeChoiceSerializer(serializers.Serializer):
    id = serializers.CharField()
    name = serializers.CharField()
