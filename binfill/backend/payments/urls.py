from django.conf.urls import include, url
from rest_framework_nested import routers
from .views import  payment_details

router = routers.SimpleRouter(trailing_slash=False)
router.register('payment_details', payment_details.PaymentDetailsViewSet, 'payment_details')
router.register('subcription_fee_frequency', payment_details.SubscriptionFeeFrequencyViewSet,
                'subcription_fee_frequency')

"""
Payment module urls - starts with 'payments'

"""

urlpatterns = [
    url(r'^', include(router.urls)),
    url('payment_choices', payment_details.PaymentChoicesViewSet.as_view())
]
