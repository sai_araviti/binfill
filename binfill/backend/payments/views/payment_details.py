# Create your views here.
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import generics
from payments.models import payment_details as payment_details_models, PaymentDetails
from payments.serializers import payment_details
from payments.serializers.helpers import PaymentTypeChoiceSerializer
from utils.shortcuts import choice_to_list_of_dict


class PaymentDetailsViewSet(viewsets.ModelViewSet):
    """ Company payments details viewset"""

    queryset = payment_details_models.PaymentDetails.objects.all()
    serializer_class = payment_details.PaymentDetailsSerializer
    http_method_names = ['get', 'post', 'put']

    def get_serializer_class(self):

        if self.action == 'retrieve' or self.action == 'list':
            serializer = payment_details.PaymentDetailsListSerializer
        else:
            serializer = payment_details.PaymentDetailsSerializer
        return serializer


class PaymentChoicesViewSet(generics.ListAPIView):
    """Listing Payment Choices Viewset"""

    serializer_class = PaymentTypeChoiceSerializer
    queryset = choice_to_list_of_dict(PaymentDetails.SUBSCRIPTION_FEE_CHOICES)

class SubscriptionFeeFrequencyViewSet(viewsets.ViewSet):
    """Listing Subscription fee Choices Viewset"""

    def list(self, request):
        dict_values = dict(PaymentDetails.SUBSCRIPTION_FEE_CHOICES)
        return Response([{i: dict_values[i]} for i in dict_values])
