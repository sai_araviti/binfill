from __future__ import unicode_literals
import os
import random
import string
import re

class RandomPasswordgenerator():

    """ Generate password ramdomly """

    def password_generator(self):

        """ funcy=tion to generatee password """

        length = 8
        chars = string.ascii_letters + string.digits

        line = re.sub('[0o0iI]', 'S', chars)
        random.seed = (os.urandom(1024))
        return ''.join(random.choice(line) for i in range(length))
