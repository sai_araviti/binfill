# Create your views here.
from rest_framework import viewsets, filters
import django_filters.rest_framework
from cities.models import *
from rest_framework.generics import CreateAPIView
from rest_framework.parsers import FileUploadParser

import utils.serializers.cities
from users.filters import cityfilter
from utils import serializers as utils_serializers
from utils.serializers.temporary_image import TemporaryImageSerializer


class CountryViewSet(viewsets.ModelViewSet):

	queryset = Country.objects.all()
	serializer_class = utils.serializers.cities.CountrySerializer
	filter_backends = (django_filters.rest_framework.DjangoFilterBackend, filters.OrderingFilter,)
	filterset_class = cityfilter.CountryFilterSet
	http_method_names = ['get']

class ProvinceViewSet(viewsets.ModelViewSet):

	queryset = Region.objects.all()
	serializer_class = utils.serializers.cities.ProvinceSerializer
	filter_backends = (django_filters.rest_framework.DjangoFilterBackend, filters.OrderingFilter,)
	filterset_class = cityfilter.ProvinceFilterSet
	http_method_names = ['get']

class CityViewSet(viewsets.ModelViewSet):

    queryset = City.objects.all()
    serializer_class = utils.serializers.cities.CitySerializer
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend, filters.OrderingFilter,)
    filterset_class = cityfilter.CityFilterSet
    http_method_names = ['get']
    

class TemporaryImageView(CreateAPIView):
	parser_classes = (FileUploadParser,)
	serializer_class = TemporaryImageSerializer