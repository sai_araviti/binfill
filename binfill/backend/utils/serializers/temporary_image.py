from rest_framework import serializers

from utils.models.tempfile import TemporaryFile


class TemporaryImageSerializer(serializers.ModelSerializer):
    choices = (('Company Logo','/company/logos/'),
               )
    upload_to = serializers.ChoiceField(choices=choices)
    file_object = serializers.ImageField()

    class Meta:
        model = TemporaryFile
        fields = ('upload_to', 'file_object')