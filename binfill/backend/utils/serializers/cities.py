from cities.models import Country, Region, City
from rest_framework import serializers


class CountrySerializer(serializers.ModelSerializer):

    class Meta:
        model = Country
        fields = ('id', 'name')


class ProvinceSerializer(serializers.ModelSerializer):

    class Meta:

        model = Region
        fields = ('id', 'name')


class CitySerializer(serializers.ModelSerializer):

    class Meta:

        model = City
        fields = ('id', 'name')