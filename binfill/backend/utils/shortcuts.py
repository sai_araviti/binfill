from random import choice

from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
import requests
from django.conf import settings
import time


class GenericSuccessSerializer(serializers.Serializer):
    data = serializers.DictField()
    message = serializers.CharField()


class GenericFailureSerializer(serializers.Serializer):
    errors = serializers.DictField()
    message = serializers.CharField()


def success(data=None, message=None, status=None, many=False):
    """
    a successfull message from the api
    """
    return Response(GenericSuccessSerializer({
        'data': data,
        'message': message,
        'status': status
    }, many=many).data)


def failure(errors=None, error_message="", code=400):
    """
    errors: could be the list all errors that went wrong,
    error_message: a generic exception why this happened
    """
    raise ValidationError(GenericFailureSerializer({
        'errors': errors,
        'message': error_message
    }).data, code=code)


def get_host(request):
    return request.META.get('HTTP_HOST', '')


def generate_random_string(length=8, numbers_only=False):
    """
    Generates a random string
    """
    numbers = "123456789"
    string = "THEQUICKBROWNFOXJUMPSOVERTHELAZYDOGthequickbrownfoxjumpsoverthelazydog.-_=$@#!" + numbers
    l = lambda: choice(list(numbers if numbers_only else string))
    return ''.join(l() for s in range(length))


def generate_random_number(length):
    return int(generate_random_string(length, numbers_only=True))


class AttributeDict(dict):
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__

def choice_to_list_of_dict(choices):
    return [AttributeDict(id=key, name=val) for key, val in choices]

def get_geo_address(latitude, longitude, sensor=True):
    base = "https://maps.googleapis.com/maps/api/geocode/json?"
    params = "latlng={lat},{lon}&sensor={sen}&key={key}".format(
        lat=latitude,
        lon=longitude,
        sen=sensor,
        key=settings.GEO_API_KEY
    )
    url = "{base}{params}".format(base=base, params=params)
    sleep_count = 5
    while (sleep_count > 0):
        result = ""
        response = requests.get(url).json()
        if (response['status'] == 'OK'):
            result = response['results'][0]['formatted_address']
            break
        else:
            sleep_count -= 1
            time.sleep(5)

    return result