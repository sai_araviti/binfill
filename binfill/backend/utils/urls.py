from django.conf.urls import include, url
from rest_framework_nested import routers

from utils.views import TemporaryImageView
from . import  views

router = routers.SimpleRouter(trailing_slash=False)
router.register('country', views.CountryViewSet, 'country')
router.register('province', views.ProvinceViewSet, 'province')
router.register('city', views.CityViewSet, 'city')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^upload-temp-image/$', TemporaryImageView.as_view())
]

