from django.db import models


class TemporaryFile(models.Model):
    """
    a temporary file to backend
    """

    file_object = models.FileField(blank=False, null=False)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return str(self.id)
