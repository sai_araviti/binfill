from cities.models import Country, Region, City
from django.db import models


class AddressMixin(models.Model):
    street = models.CharField(blank=False, max_length=100)
    country = models.ForeignKey(Country, db_index=True, on_delete=models.PROTECT, related_name="%(app_label)s_%(class)s_related")
    province = models.ForeignKey(Region, db_index=True, on_delete=models.PROTECT, related_name="%(app_label)s_%(class)s_related")
    city = models.ForeignKey(City, db_index=True, on_delete=models.PROTECT, related_name="%(app_label)s_%(class)s_related")
    postal_code = models.CharField(max_length=10)

    class Meta:
        abstract = True