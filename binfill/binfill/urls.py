"""binfill URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import include, url
from django.urls import path
from binFill_Login import urls as Login_urls
from binFill_Order import urls as Order_urls
from binFill_Users import urls as Users_urls
from binFill_Bins import urls as Bins_urls
from binFill_Trucks import urls as Trucks_urls
from binFill_DumpSites import urls as DumpSites_urls
from binFill_Customers import urls as Customers_urls
from binFill_Accounts import urls as Accounts_urls
from binFill_Reports import urls as Reports_urls
from binFill_Settings import urls as Settings_urls


urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'binFill/', include(Login_urls)),
    url(r'binFill/', include(Order_urls)),
    url(r'binFill/', include(Users_urls)),
    url(r'binFill/', include(Bins_urls)),
    url(r'binFill/', include(Trucks_urls)),
    url(r'binFill/', include(DumpSites_urls)),
    url(r'binFill/', include(Customers_urls)),
    url(r'binFill/', include(Accounts_urls)),
    url(r'binFill/', include(Reports_urls)),
    url(r'binFill/', include(Settings_urls)),
]
